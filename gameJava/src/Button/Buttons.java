package Button;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class Buttons extends GridPane {
	public Button rButton, bButton, gButton, yButton;

	public Buttons() {
		rButton = new Button();
		rButton.setStyle("-fx-background-color: #ff0000");
		rButton.setPrefWidth(50);
		rButton.setPrefHeight(50);
		
		gButton = new Button();
		gButton.setStyle("-fx-background-color: #00ff0c");
		gButton.setPrefWidth(50);
		gButton.setPrefHeight(50);

		bButton = new Button();
		bButton.setStyle("-fx-background-color: #0004ff");
		bButton.setPrefWidth(50);
		bButton.setPrefHeight(50);

		yButton = new Button();
		yButton.setStyle("-fx-background-color: #fffa00");
		yButton.setPrefWidth(50);
		yButton.setPrefHeight(50);
	
		addGrid();
	}
	
	public void addGrid() {
		this.getChildren().clear();
		this.setPadding(new Insets(1, 1, 1, 50));
		this.setHgap(50);
		this.setVgap(40);
		this.add(gButton, 0, 0);
		this.add(bButton, 1, 0);
		this.add(rButton, 0, 1);
		this.add(yButton, 1, 1);
	}


}

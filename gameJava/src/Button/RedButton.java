package Button;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import MainMenu.Administrator;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class RedButton extends Application implements EventHandler<ActionEvent> {
	private Scene scene;
	private GridPane root;
	private Button RedButtonH;
	private Label mensaje;
	private Font font;
	private Label terminal;
	private Buttons buttons;
	private Stage stage;
	private int vidasGeneral;
	private int dificultad;
	private int scoreGeneral;
	private ArrayList<String>textos;
	private int textoElegido;
	public RedButton() {
		leerTipografia();
		textos= new ArrayList<String>();
		textos.add("Al rojo lo que es de el rojo");//
		textos.add("Porque no el rojo mas peque�o");//
		textos.add("Solo el azul debe ser el correcto");//
		textos.add("Solo preocupate por aquel color que junto a el azul produce verde");//
		textos.add("rojo de cuatro puntas");//
		textos.add("no ROJO, no VERDE, no AMARILLO,solo azul");
		textos.add("Azul");
		textos.add("Hojas de arbol debe ser");
		textos.add("Si ganar quieres, entender la referencia debes");
		textos.add("A. . . . . . O");
		textos.add("luzale");
		textos.add("ONojor, amarillos");
		textos.add("R V A");


		root = new GridPane();
		scene = new Scene(root, 1200, 950);
		terminal = new Label("T3rm1N4L\n&3 dƒÆrS\n-->");
		terminal.setFont(font);
		terminal.setTextFill(Color.web("#5b5a5a"));
		textoElegido=(int)((Math.random()*textos.size()));
		mensaje = new Label(">> "+textos.get(12));
		mensaje.setFont(font);
		mensaje.setTextFill(Color.web("black"));
		buttons = new Buttons();
		RedButtonH = new Button();
		RedButtonH.setStyle("-fx-background-color: Transparent");
		RedButtonH.setPrefWidth(150);
		RedButtonH.setPrefHeight(230);		
		buttons.bButton.setOnAction(this);
		buttons.rButton.setOnAction(this);
		buttons.gButton.setOnAction(this);
		buttons.yButton.setOnAction(this);
	}
	@Override
	public void handle(ActionEvent botones) {
		if(textoElegido==0)
		{
			if (botones.getSource().equals(RedButtonH)) {
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}
		else if(textoElegido==1)
		{
			if(botones.getSource().equals(buttons.rButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}
		else if(textoElegido==2)
		{
			if(botones.getSource().equals(buttons.bButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}
		else if(textoElegido==3)
		{
			if(botones.getSource().equals(buttons.yButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();		}
		else if(textoElegido==4)
		{
			if(botones.getSource().equals(buttons.rButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}
		else if(textoElegido==5)
		{
			if(botones.getSource().equals(buttons.bButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}
		else if(textoElegido==6)
		{
			if(botones.getSource().equals(buttons.bButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}
		else if(textoElegido==7)
		{
			if(botones.getSource().equals(buttons.gButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}
		else if(textoElegido==8)
		{
			if(botones.getSource().equals(buttons.gButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}
		else if(textoElegido==9)
		{
			if(botones.getSource().equals(buttons.yButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}
		else if(textoElegido==10)
		{
			if(botones.getSource().equals(buttons.bButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}else if(textoElegido==11)
		{
			if(botones.getSource().equals(buttons.yButton))
			{
				mensaje.setText(">>BIEN HECHO...\t");
				salir();
			}
			perder();
		}
		else if(textoElegido==12)
		{
			if(botones.getSource().equals(buttons.rButton))
			{
				if(botones.getSource().equals(buttons.gButton))
				{

					if(botones.getSource().equals(buttons.yButton))
					{

						mensaje.setText(">>BIEN HECHO...\t");
						salir();
					}
					perder();
				}
				perder();
			}
			perder();
		}
		perder();








	}
	public void salir()
	{
		Administrator admin = new Administrator();
		try {
			admin.iniciateGame((int)(Math.random()*admin.getGames().size()), stage);
			admin.setScore(admin.getScore()+1);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}
	@Override
	public void start(Stage RedButton) {
		long mTime = System.currentTimeMillis(); 
		long end = mTime + 10*1000; // 5 seconds 
		Administrator admin = new Administrator();
		if(System.currentTimeMillis()>end)
		{
			try {
				ingresar(vidasGeneral+1,dificultad,scoreGeneral);
				admin.iniciateGame((int)(Math.random()*admin.getGames().size()), stage);

			} catch (Exception e) {

				e.printStackTrace();
			}
		}
		margenes();
		addGrid();
		RedButtonH.setOnAction(this);
		RedButton.setScene(scene);
		RedButton.setTitle("RedButton");
		stage= RedButton;
		RedButton.show();

	}
	public void perder()
	{
		root.getChildren().clear();
		BackgroundImage myBI = new BackgroundImage(new Image("Button/GameOver2.png", 1200, 950, false, true),BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,BackgroundSize.DEFAULT);
		root.setBackground(new Background(myBI));
	}
	public void margenes() {
		root.setPadding(new Insets(100, 100, 100, 100));
		root.setHgap(30);
		root.setVgap(10);
	}
	public void addGrid() {
		root.getChildren().clear();
		BackgroundImage myBI = new BackgroundImage(new Image("Button/Fondo.png", 1200, 950, false, true),BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,BackgroundSize.DEFAULT);
		root.setBackground(new Background(myBI));
		GridPane.setHalignment(buttons, HPos.CENTER);
		root.add(buttons, 8, 0);
		GridPane.setHalignment(RedButtonH, HPos.CENTER);
		root.add(RedButtonH, 4, 8);
		GridPane.setHalignment(mensaje, HPos.LEFT);
		root.add(mensaje, 8, 8);
		GridPane.setHalignment(terminal, HPos.CENTER);
		root.add(terminal, 6, 8);
	}
	public void leerTipografia() {
		try {
			font = Font.loadFont(new FileInputStream(new File(
					"Button/8bitOperatorPlus-Bold.ttf")),
					20);
		} catch (FileNotFoundException e) {
			System.out.println("no se pudo cargar el archivo");
			font = new Font("Arial", 20);
		}

	}


	public static void main(String[] args) {
		launch(args);
	}
	public void leerDatos() {
		Scanner sc;
		try {
			sc = new Scanner(new File("datos.txt"));
			while(sc.hasNext())
			{
				System.out.println("lklk");
				vidasGeneral=sc.nextInt();
				dificultad=sc.nextInt();
				scoreGeneral=sc.nextInt();
			}
			System.out.println("hol");
		} catch (Exception e) {	
			System.out.println("ERROR");
		}
	}	

	public void ingresar(int x,int y, int z){
		FileWriter escritorArchivo;
		try {
			System.out.println("adhsad");
			escritorArchivo = new FileWriter("datos.txt", true);
			escritorArchivo.append((x+1) +" "+y+" "+z);

			escritorArchivo.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

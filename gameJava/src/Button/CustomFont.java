package Button;

import java.io.File;
import java.io.FileInputStream;

import javafx.scene.text.Font;

public class CustomFont {

	private Font font = null;

	public CustomFont() {
		try {
			font = Font.loadFont(new FileInputStream(new File(
					"Button/8bitOperatorPlus-Bold.ttf")),
					12);

		} catch (Exception ex) {
			// Si existe un error se carga fuente por defecto ARIAL
			System.err.println(" No se cargo la fuente");
			font = new Font("Arial", 14);
		}
	}

	public Font MyFont() {
		Font tfont = font;
		return tfont;
	}

}
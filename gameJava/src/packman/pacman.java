package packman;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class pacman extends Application {

	// *-muros
	// .-pepitas
	// +-pepitasOP
	// "/"-vacio
	// R-Red
	// "--" entrada y salida de la casita
	// %- tp
	// B -blue
	// P-pink
	// O-orange
	// p-pacman

	private Scene scene;
	private Stage primaryStage;
	private Pane pane;
	public char[][] tablero = {
			{ '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*','*', '*', '*', '*', '*', '*', '*' },
			{ '*', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '*', '*', '.', '.', '.', '.', '.', '.','.', '.', '.', '.', '.', '.', '*' },
			{ '*', '.', '*', '*', '*', '*', '.', '*', '*', '*', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*','.', '*', '*', '*', '*', '.', '*' },
			{ '*', '+', '*', '*', '*', '*', '.', '*', '*', '*', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*','.', '*', '*', '*', '*', '+', '*' },
			{ '*', '.', '*', '*', '*', '*', '.', '*', '*', '*', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*','.', '*', '*', '*', '*', '.', '*' },
			{ '*', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.','.', '.', '.', '.', '.', '.', '*' },
			{ '*', '.', '*', '*', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*', '*', '*', '*', '.', '*', '*','.', '*', '*', '*', '*', '.', '*' },
			{ '*', '.', '*', '*', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*', '*', '*', '*', '.', '*', '*','.', '*', '*', '*', '*', '.', '*' },
			{ '*', '.', '.', '.', '.', '.', '.', '*', '*', '.', '.', '.', '.', '*', '*', '.', '.', '.', '.', '*', '*','.', '.', '.', '.', '.', '.', '*' },
			{ '*', '*', '*', '*', '*', '*', '.', '*', '*', '*', '*', '*', '/', '*', '*', '/', '*', '*', '*', '*', '*','.', '*', '*', '*', '*', '*', '*' },
			{ '/', '/', '/', '/', '/', '*', '.', '*', '*', '*', '*', '*', '/', '*', '*', '/', '*', '*', '*', '*', '*','.', '*', '/', '/', '/', '/', '/' },
			{ '/', '/', '/', '/', '/', '*', '.', '*', '*', '/', '/', '/', '/', 'R', '/', '/', '/', '/', '/', '*', '*','.', '*', '/', '/', '/', '/', '/' },
			{ '/', '/', '/', '/', '/', '*', '.', '*', '*', '/', '*', '*', '*', '-', '-', '*', '*', '*', '/', '*', '*','.', '*', '/', '/', '/', '/', '/' },
			{ '*', '*', '*', '*', '*', '*', '.', '*', '*', '/', '*', '/', '/', '/', '/', '/', '/', '*', '/', '*', '*','.', '*', '*', '*', '*', '*', '*' },
			{ '%', '/', '/', '/', '/', '/', '.', '/', '/', '/', '*', 'B', '/', 'P', '/', 'O', '/', '*', '/', '/', '/','.', '/', '/', '/', '/', '/', '%' },
			{ '*', '*', '*', '*', '*', '*', '.', '*', '*', '/', '*', '/', '/', '/', '/', '/', '/', '*', '/', '*', '*','.', '*', '*', '*', '*', '*', '*' },
			{ '/', '/', '/', '/', '/', '*', '.', '*', '*', '/', '*', '*', '*', '*', '*', '*', '*', '*', '/', '*', '*','.', '*', '/', '/', '/', '/', '/' },
			{ '/', '/', '/', '/', '/', '*', '.', '*', '*', '/', '/', '/', '/', '/', '/', '/', '/', '/', '/', '*', '*','.', '*', '/', '/', '/', '/', '/' },
			{ '/', '/', '/', '/', '/', '*', '.', '*', '*', '/', '*', '*', '*', '*', '*', '*', '*', '*', '/', '*', '*','.', '*', '/', '/', '/', '/', '/' },
			{ '*', '*', '*', '*', '*', '*', '.', '*', '*', '/', '*', '*', '*', '*', '*', '*', '*', '*', '/', '*', '*','.', '*', '*', '*', '*', '*', '*' },
			{ '*', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '*', '*', '.', '.', '.', '.', '.', '.','.', '.', '.', '.', '.', '.', '*' },
			{ '*', '.', '*', '*', '*', '*', '.', '*', '*', '*', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*','.', '*', '*', '*', '*', '.', '*' },
			{ '*', '.', '*', '*', '*', '*', '.', '*', '*', '*', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*','.', '*', '*', '*', '*', '.', '*' },
			{ '*', '+', '.', '.', '*', '*', '.', '.', '.', '.', '.', '.', '.', 'p', '.', '.', '.', '.', '.', '.', '.','.', '*', '*', '.', '.', '+', '*' },
			{ '*', '*', '*', '.', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*', '*', '*', '*', '.', '*', '*','.', '*', '*', '.', '*', '*', '*' },
			{ '*', '*', '*', '.', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*', '*', '*', '*', '.', '*', '*','.', '*', '*', '.', '*', '*', '*' },
			{ '*', '.', '.', '.', '.', '.', '.', '*', '*', '.', '.', '.', '.', '*', '*', '.', '.', '.', '.', '*', '*','.', '.', '.', '.', '.', '.', '*' },
			{ '*', '.', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*','*', '*', '*', '*', '*', '.', '*' },
			{ '*', '.', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '.', '*', '*', '.', '*', '*', '*', '*', '*','*', '*', '*', '*', '*', '.', '*' },
			{ '*', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.','.', '.', '.', '.', '.', '.', '*' },
			{ '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*','*', '*', '*', '*', '*', '*', '*' },
	};
	public static	Image foto = new Image("packman/pacman1D.png",32,32,true,true);
	public static ImageView pacman = new ImageView(foto);

	public void tab(char[][] T, Stage primaryStage, Pane pane) {
		for (int i = 0; i <T.length; i++) {
			for (int j = 0; j < T[0].length; j++) {
				if (T[i][j] == 'p') {
					Image foto = new Image("packman/pacman1L.png", 20, 20, true, true);
					ImageView pacman = new ImageView(foto);
					pacman.setLayoutX(i * 20);
					pacman.setLayoutY(j * 20);
					pane.getChildren().add(pacman);
				}
				if (T[i][j] == '*') {
					Image muro = new Image("packman/muro.jpg", 15, 15, true, true);
					ImageView Muro = new ImageView(muro);
					Muro.setLayoutX(i * 20);
					Muro.setLayoutY(j * 20);
					pane.getChildren().add(Muro);
				}
				if (T[i][j] == 'P') {
					Image pink = new Image("packman/pinky 3.png", 20, 20, true, true);
					ImageView Pinky = new ImageView(pink);
					Pinky.setLayoutX(i * 20);
					Pinky.setLayoutY(j * 20);
					pane.getChildren().add(Pinky);
				}
				if (T[i][j] == '.') {
					Image pepita = new Image("packman/Pepita.PNG", 10, 10, true, true);
					ImageView p1 = new ImageView(pepita);
					p1.setLayoutX(i * 20);
					p1.setLayoutY(j * 20);
					pane.getChildren().add(p1);
				}
				if (T[i][j] == 'B') {
					Image azul = new Image("packman/azul 3.png", 20, 20, true, true);
					ImageView Azul = new ImageView(azul);
					Azul.setLayoutX(i * 20);
					Azul.setLayoutY(j * 20);
					pane.getChildren().add(Azul);
				}
				if (T[i][j] == 'O') {
					Image orange = new Image("packman/orange 4.png", 20, 20, true, true);
					ImageView Orange = new ImageView(orange);
					Orange.setLayoutX(i * 20);
					Orange.setLayoutY(j * 20);
					pane.getChildren().add(Orange);
				}
				if (T[i][j] == '/') {
					Image transparente = new Image("packman/negro.png", 20, 20, true, true);
					ImageView t = new ImageView(transparente);
					t.setLayoutX(i * 20);
					t.setLayoutY(j * 20);
					pane.getChildren().add(t);
				}
				if (T[i][j] == 'R') {
					Image red = new Image("packman/red 1.png", 20, 20, true, true);
					ImageView Red = new ImageView(red);
					Red.setLayoutX(i * 20);
					Red.setLayoutY(j * 20);
					pane.getChildren().add(Red);
				}
				if (T[i][j] == '+') {
					Image pepitaOP = new Image("packman/Pepita.PNG", 20, 20, true, true);
					ImageView POP = new ImageView(pepitaOP);
					POP.setLayoutX(i * 20);
					POP.setLayoutY(j * 20);
					pane.getChildren().add(POP);
				}
			}

		}
		primaryStage.show();
	}

	public void arriba() {
		boolean continuar = true;
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {

				if (tablero[i][j] == 'p' && continuar == true) {
					if (tablero[i][j - 1] != '*') {
						tablero[i][j] = '/';
						tablero[i][j - 1] = 'p';
						tab(tablero,primaryStage,pane);
						continuar = false;
					}

				}


			}
		}
	}

	public void abajo() {
		boolean continuar = true;
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				if (tablero[i][j] == 'p'&& continuar == true) {
					if (tablero[i][j + 1] != '*') {
						tablero[i][j] = '/';
						tablero[i][j + 1] = 'p';
						tab(tablero,primaryStage,pane);
						continuar = false;
					}

				}
			}
		}
	}

	public void izquierda() {
		boolean continuar = true;
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				if (tablero[i][j] == 'p'&& continuar == true) {
					if (tablero[i-1][j] != '*') {
						tablero[i][j] = '/';
						tablero[i-1][j] = 'p';
						tab(tablero,primaryStage,pane);
						continuar = false;
					}

				}
			}
		}
	}

	public void derecha() {
		
		boolean continuar = true;
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				if (tablero[i][j] == 'p'&& continuar == true) {
					if (tablero[i+1][j] != '*') {
						tablero[i][j] = '/';
						tablero[i+1][j] = 'p';
						tab(tablero,primaryStage,pane);
						continuar = false;
					}

				}
			}
		}
	}

	@Override
	public void start(Stage primarystage) throws Exception {
		primaryStage = primarystage;		
		pane = new Pane();
		pane.setPrefSize(620, 560);
		tab(tablero, primarystage, pane);
		scene = new Scene(pane);
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			//@Override
			public void handle(KeyEvent e) {
				
				if(e.getCode()==KeyCode.LEFT){
					if(System.currentTimeMillis()%90<30){
						Image foto = new Image("packman/pacman1L.png",20,20,true,true);
						pacman.setImage(foto);				
					}
					if (System.currentTimeMillis()%90>30 && System.currentTimeMillis()%90<60){
						Image foto = new Image("packman/pacman2L.png",20,20,true,true);
						pacman.setImage(foto);				
					}
					if(System.currentTimeMillis()%90>60 && System.currentTimeMillis()%90<100){
						Image foto = new Image("packman/pacman3L.png",20,20,true,true);
						pacman.setImage(foto);	
					}
					pacman.setLayoutX(pacman.getLayoutX()-3);
					izquierda();

				}
				if(e.getCode()==KeyCode.RIGHT){
					derecha();

					if(System.currentTimeMillis()%90<30){
						Image foto = new Image("packman/pacman1R.png",20,20,true,true);
						pacman.setImage(foto);				
					}
					if (System.currentTimeMillis()%90>30 && System.currentTimeMillis()%90<60){
						Image foto = new Image("packman/pacman2R.png",20,20,true,true);
						pacman.setImage(foto);				
					}
					if(System.currentTimeMillis()%90>60 && System.currentTimeMillis()%90<100){
						Image foto = new Image("packman/pacman3R.png",20,20,true,true);
						pacman.setImage(foto);	
					}
					pacman.setLayoutX(pacman.getLayoutX()+3);
					derecha();
				}
				if(e.getCode()==KeyCode.DOWN){

					if(System.currentTimeMillis()%90<30){
						Image foto = new Image("packman/pacman1D.png",20,20,true,true);
						pacman.setImage(foto);				
					}
					if (System.currentTimeMillis()%90>30 && System.currentTimeMillis()%90<60){
						Image foto = new Image("packman/pacman2D.png",20,20,true,true);
						pacman.setImage(foto);				
					}
					if(System.currentTimeMillis()%90>60 && System.currentTimeMillis()%90<100){
						Image foto = new Image("packman/pacman3D.png",20,20,true,true);
						pacman.setImage(foto);	
					}
					pacman.setLayoutY(pacman.getLayoutY()+3);
					abajo();
				}
				if(e.getCode()==KeyCode.UP){

					if(System.currentTimeMillis()%90<30){
						Image foto = new Image("packman/pacman1U.png",20,20,true,true);
						pacman.setImage(foto);				
					}
					if (System.currentTimeMillis()%90>30 && System.currentTimeMillis()%90<60){
						Image foto = new Image("packman/pacman2U.jpg",20,20,true,true);
						pacman.setImage(foto);				
					}
					if(System.currentTimeMillis()%90>60 && System.currentTimeMillis()%90<100){
						Image foto = new Image("packman/pacman3U.png",20,20,true,true);
						pacman.setImage(foto);	
					}
					pacman.setLayoutY(pacman.getLayoutY()-3);
					arriba();
				}

			}
		});
		scene.setFill(Color.BLACK);
		primaryStage.setScene(scene);

	}

	public static void main(String[] args) {
		Application.launch(args);
	}

}

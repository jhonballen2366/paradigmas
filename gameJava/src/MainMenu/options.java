package MainMenu;


import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class options extends Application {

	private static final Font FONT = Font.font("", FontWeight.BOLD, 12);

	private VBox menuBox;
	private int currentItem = 0;
	//Encontre el ScheduledExecutorService en las librerias, basicamente es un ejecutador... FIN.
	private ScheduledExecutorService bgThread = Executors.newSingleThreadScheduledExecutor();





	private Parent createContent() {
		Pane root = new Pane();
		root.setPrefSize(900, 600);

		Rectangle bg = new Rectangle(900, 600);

		ContentFrame frame1 = new ContentFrame(createLeftContent());
		ContentFrame frame2 = new ContentFrame(createMiddleContent());
		ContentFrame frame3 = new ContentFrame(createRightContent());

		HBox hbox = new HBox(15, frame1, frame2, frame3);
		hbox.setTranslateX(120);
		hbox.setTranslateY(50);


		menuBox = new VBox(10,
				//cada uno de estos es un item, un botton :D
				new MenuItem("DIFICULTAD "),
				new MenuItem("Facil"),
				new MenuItem("Medio"),
				new MenuItem("Dificil"),
				new MenuItem("INHUMANO"),
				new MenuItem("REGRESAR")


				);
		menuBox.setAlignment(Pos.TOP_CENTER);
		menuBox.setTranslateX(360);
		menuBox.setTranslateY(300);

		Text about = new Text("Establece la dificultad");
		about.setTranslateX(50);
		about.setTranslateY(500);
		about.setFill(Color.WHITE);
		about.setFont(FONT);
		about.setOpacity(0.2);

		getMenuItem(0).setActive(true);

		root.getChildren().addAll(bg, hbox, menuBox, about);
		return root;
	}

	private Node createLeftContent() {
		ArrayList<String>a= new ArrayList<String>();
		a.add("Lights Off \n llega a la meta sin perder tu mana.(flechas)");
		a.add("Da Button \n lee bien (click).");
		a.add("Haunt \n recupera el dinero (flechas).");
		a.add("Boom \n sigue las flechas.");
		a.add("Taiko \n sigue el ritmo! A y D para las notas.");
		a.add("Pez \n Esquiva la basura, toma las vidas.");
		a.add("Boom \n sigue las flechas.");
		a.add("SpaceAtack \n defiende.");

		int selectedText=(int)((Math.random()*a.size()));;
		final Text inside = new Text("Come on! join us 7u7");
		inside.setFill(Color.WHITE);

		bgThread.scheduleAtFixedRate(() -> {
			Platform.runLater(() -> {
				TranslateTransition tt = new TranslateTransition(Duration.seconds(0.5), inside);
				tt.setToY(150);

				FadeTransition ft = new FadeTransition(Duration.seconds(0.5), inside);
				ft.setToValue(0);

				ParallelTransition pt = new ParallelTransition(tt, ft);
				pt.setOnFinished(e -> {
					inside.setTranslateY(-200);
					inside.setText(a.get(selectedText));

					TranslateTransition tt2 = new TranslateTransition(Duration.seconds(0.5), inside);
					tt2.setToY(0);

					FadeTransition ft2 = new FadeTransition(Duration.seconds(0.5), inside);
					ft2.setToValue(1);

					ParallelTransition pt2 = new ParallelTransition(tt2, ft2);
					pt2.play();
				});
				pt.play();
			});
		}, 2, 10, TimeUnit.SECONDS);

		return inside;
	}

	private Node createMiddleContent() {
		String title = "CONFIGURA TU PARTIDA!";
		HBox letters = new HBox(0);
		letters.setAlignment(Pos.CENTER);
		//ESTO ES PARA EL EFECTO DE TRANSLUCIDAD
		for (int i = 0; i < title.length(); i++) {
			Text letter = new Text(title.charAt(i) + ""/*<-- si se quitan esas comillas no compila ojo*/);
			letter.setFont(FONT);
			letter.setFill(Color.WHITE);
			letters.getChildren().add(letter);

			TranslateTransition tt = new TranslateTransition(Duration.seconds(3), letter);
			tt.setDelay(Duration.millis(i * 75));
			tt.setToY(-30);
			tt.setAutoReverse(true);
			tt.setCycleCount(TranslateTransition.INDEFINITE);
			tt.play();
		}

		return letters;
	}

	private Node createRightContent() {
		ArrayList<String>a= new ArrayList<String>();
		a.add("Lights Off \n llega a la meta sin perder tu mana.\n(flechas)");
		a.add("Da Button \n lee bien (click).");
		a.add("Haunt \n recupera el dinero \n (flechas).");
		a.add("Boom \n sigue las flechas.");
		a.add("Taiko \n sigue el ritmo!\n A y D para las notas.");
		a.add("Pez \n Esquiva la basura\n toma las vidas.");
		a.add("Boom \n sigue las flechas.");

		int selectedText=(int)((Math.random()*a.size()));;

		String title = a.get(selectedText);
		HBox letras = new HBox(0);
		letras.setAlignment(Pos.CENTER);
		for (int i = 0; i < title.length(); i++) {
			Text letter = new Text(title.charAt(i)+""/*<-- si se quitan esas comillas no compila ojo*/);
			letter.setFont(FONT);
			letter.setFill(Color.WHITE);
			letter.setOpacity(0);
			letras.getChildren().add(letter);

			FadeTransition ft = new FadeTransition(Duration.seconds(2), letter);
			ft.setDelay(Duration.millis(i * 25));
			ft.setToValue(1);
			ft.setAutoReverse(true);
			ft.setCycleCount(TranslateTransition.INDEFINITE);
			ft.play();
		}

		return letras;
	}

	private MenuItem getMenuItem(int index) {
		return (MenuItem)menuBox.getChildren().get(index);
	}
	//los cuadrados alrededor de las cosas
	private static class ContentFrame extends StackPane {
		public ContentFrame(Node content) {
			setAlignment(Pos.CENTER);

			Rectangle frame = new Rectangle(200, 200);
			frame.setArcWidth(25);
			frame.setArcHeight(25);
			frame.setStroke(Color.WHITE);

			getChildren().addAll(frame, content);
		}
	}

	private static class MenuItem extends HBox {
		private Puntero c1 = new Puntero(), c2 = new Puntero();
		public Text text;
		private Runnable scriptRunner;

		public MenuItem(String name) {
			super(15);
			setAlignment(Pos.CENTER);

			text = new Text(name);
			text.setFont(FONT);
			text.setEffect(new GaussianBlur(2));

			getChildren().addAll(c1, text, c2);
			setActive(false);
			setOnActivate(() -> System.out.println(name + "establecido como dificultad"));


		}
		//metodo de activacion!
		public void setActive(boolean b) {
			c1.setVisible(b);
			c2.setVisible(b);
			text.setFill(b ? Color.WHITE : Color.GREY);
		}

		public void setOnActivate(Runnable r) {
			scriptRunner = r;
		}

		public void activate() {
			if (scriptRunner != null)
				scriptRunner.run();
		}
		public Text getText() {
			return text;
		}
		public void setText(Text text) {
			this.text = text;
		}


	}

	private static class Puntero extends Parent {
		public Puntero() {
			//ESTE ES EL PUNTERO NO SE CONFUNDAN
			Shape figura1 = Shape.subtract(new Circle(5), new Circle(2));
			figura1.setFill(Color.WHITE);

			Shape figura2 = Shape.subtract(new Circle(5), new Circle(2));
			figura2.setFill(Color.WHITE);
			figura2.setTranslateX(5);

			Shape figura3 = Shape.subtract(new Circle(5), new Circle(2));
			figura3.setFill(Color.WHITE);
			figura3.setTranslateX(2.5);
			figura3.setTranslateY(-5);

			getChildren().addAll(figura1, figura2, figura3);

			setEffect(new GaussianBlur(2));
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		mainMenuTest menu = new mainMenuTest();
		Scene scene = new Scene(createContent());
		scene.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.UP) {
				if (currentItem > 0) {
					getMenuItem(currentItem).setActive(false);
					getMenuItem(--currentItem).setActive(true);
				}
			}

			if (event.getCode() == KeyCode.DOWN) {
				if (currentItem < menuBox.getChildren().size() - 1) {
					getMenuItem(currentItem).setActive(false);
					getMenuItem(++currentItem).setActive(true);
				}
			}

			if (event.getCode() == KeyCode.ENTER) {

				if(currentItem==5)
				{
					try {
						menu.start(primaryStage);
					} catch (Exception e) {

						e.printStackTrace();
					}
				}
				getMenuItem(currentItem).activate();
			}

		});

		primaryStage.setScene(scene);
		primaryStage.setOnCloseRequest(event -> {
			bgThread.shutdownNow();
		});
		primaryStage.setTitle("options");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
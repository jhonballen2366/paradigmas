package MainMenu;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.stage.Stage;
import Lights.LigthOff;
import Button.RedButton;
import Boom.Juego;
import Taiko.Main;
import Pez.PezGames;
import Haunt.Haunt;
import SpaceAtack.Space;

public class Administrator {
	ArrayList<Application> games;
	int dif;
	int score;
	public Administrator ()
	{
		games = new ArrayList<Application>();
		games.add(new LigthOff());
		games.add(new RedButton());
		games.add(new Juego());
		//taiko
		games.add(new Main());
		games.add(new PezGames());
		games.add(new Haunt());
		games.add(new Space());
		score=0;
		
		 
	}
	
	public int getDif() {
		return dif;
	}

	public void setDif(int dif) {
		this.dif = dif;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public ArrayList<Application> getGames() {
		return games;
	}
	public void setGames(ArrayList<Application> games) {
		this.games = games;
	}
	public void iniciateGame(int i,Stage args) throws Exception{
		if(i>games.size())
		{
			System.out.println("Sorry no game founded");
		}
		else{
			games.get(i).start(args);
		}
		
	}
	
}

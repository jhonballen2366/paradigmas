package Lights;	
	import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
	import java.util.TimerTask;

import MainMenu.Administrator;
import javafx.animation.AnimationTimer;
	import javafx.event.EventHandler;
	import javafx.scene.Group;
	import javafx.scene.Scene;
	import javafx.scene.canvas.Canvas;
	import javafx.scene.canvas.GraphicsContext;
	import javafx.scene.control.ProgressBar;
	import javafx.scene.image.Image;
	import javafx.scene.input.KeyCode;
	import javafx.scene.input.KeyEvent;
	import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
	import javafx.scene.text.Text;
import javafx.stage.Stage;
	
	/**
	 * Esta clase se encarga de ejecutar cada accion y funcionamiento completo del programa.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez
	 * @version: 25/05/2019
	 */
	
	//Campos de la clase 
	public class Tablero extends Scene implements EventHandler<KeyEvent> {
	
		protected Canvas canvasTablero;
		protected Player jugador1;
		protected Image fondoTablero;
		protected GraphicsContext context;
		protected Llegada llegada1;
		protected ArrayList<Enemy> enemigos;
		private ProgressBar vidaBarra;
		private Text vidaText;
		private HBox datos;
		private Stage mainStage;
		private static double vidaNum=1.0;
		private boolean helper;
		private Media musica;
		private MediaPlayer reproducir;
		private String path;
		private int vidasGeneral;
		private int dificultad;
		private int scoreGeneral;
		
	    /**
	     * Constructor para la clase del tablero principal.
	     * @param root porque se extiende a Scena
	     * @param width asigna el tama�o a la Scene
	     * @param height asigna el tama�o a la Scene
	     */
		
		
		public Tablero(Group root,double width, double height,Stage mainStage) {
			super(root);
			leerDatos();
			this.mainStage=mainStage;
			vidaBarra= new ProgressBar(vidaNum);
			vidaText = new Text("              MAGIA:");
			vidaText.setFill(Color.BEIGE);
			vidaText.setStyle("-fx-font: 40 arial;");
			vidaBarra.setStyle("-fx-font: 25 arial;");
			helper=true;
			this.path = "src/Lights/Imagenes/Bit Rush  Login Screen - League of Legends.mp3";
			this.musica =new Media (new File(path).toURI().toString());  
			this.reproducir = new MediaPlayer (musica);
			datos = new HBox();
			datos.setSpacing(25);
			datos.getChildren().addAll(vidaText,vidaBarra);
	
			canvasTablero= new Canvas(width,height);
			jugador1 = new Player();
			llegada1 = new Llegada();
			fondoTablero = new Image("Lights/Imagenes/FondoMedieval.jpg");
			enemigos = new ArrayList<Enemy> ();
			for (int i = 0; i <6; i++) {
				Enemy enemigoAux = new Enemy();
				enemigos.add(enemigoAux);
			}
	
	
	
			root.getChildren().addAll(canvasTablero, datos);
			context = canvasTablero.getGraphicsContext2D();
			this.setOnKeyPressed(this);
	
		} //Cierre del constructor
	
		   /**
		    * M�todo runLevel ejecuta el hilo en el que se correra el programa.
			*/
		public void leerDatos() {
			Scanner sc;
			try {
				sc = new Scanner(new File("datos.txt"));
				while(sc.hasNext())
				{
					System.out.println("lklk");
					vidasGeneral=sc.nextInt();
					dificultad=sc.nextInt();
					scoreGeneral=sc.nextInt();
				}
				System.out.println("hol");
			} catch (Exception e) {	
				System.out.println("ERROR");
			}
		}	
	
		public void ingresar(int x,int y, int z){
			FileWriter escritorArchivo;
			try {
				System.out.println("adhsad");
				escritorArchivo = new FileWriter("datos.txt", true);
				escritorArchivo.append((x+1) +" "+y+" "+z);
				
				escritorArchivo.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		public void runLevel() {
			Administrator admin = new Administrator();
			long mTime = System.currentTimeMillis(); 
			long end = mTime + 10*1000; // 5 seconds 

			new AnimationTimer() {
	
				@Override
				public void handle(long now) {
					if(System.currentTimeMillis()>end)
					{
						fondoTablero = new Image("Lights/Imagenes/GameOverMedieval.jpg",950,700,true,false);
						jugador1.setPosX(-200);
						llegada1.setPosY(2000);
					}
	
					Timer aux = new Timer();
	
					aux.schedule(new TimerTask(){
						@Override
						public void run() {
							for (int i = 0; i < enemigos.size(); i++) {
								enemigos.get(i).setSpriteEnemy(enemigos.get(i).getSpriteInv());;
	
							} 
	
						}
					}, 3000);
	
					context.drawImage(fondoTablero, 0, 0);
					context.drawImage(jugador1.getSpritePlayer(), jugador1.getPosX(), jugador1.getPosY());
					context.drawImage(llegada1.getLlegadaImage(), llegada1.getPosX(), llegada1.getPosY());
	
					if (vidaNum<=0) {
	
						fondoTablero = new Image("Lights/Imagenes/GameOverMedieval.jpg",950,700,true,false);
						jugador1.setPosX(-200);
						llegada1.setPosY(2000);
					}
	
					for (int i = 0; i < enemigos.size(); i++) {
	
						if ((win(llegada1,jugador1)||jugador1.getRangeX()>=650&&jugador1.getRangeY()>=430)&&helper==true) {
							helper=false;
							 try {
								 ingresar(vidasGeneral+1,dificultad,scoreGeneral);
			            		  admin.iniciateGame((int)(Math.random()*admin.getGames().size()), mainStage);
			            		  admin.setScore(admin.getScore()+1);
			      				} catch (Exception e) {
								
								e.printStackTrace();
							}	
	}
	
						if (colision(enemigos.get(i))) {
							enemigos.remove(i);
							vidaNum=vidaNum-0.4;
							vidaBarra.setProgress(vidaNum);	
	
						}
					}
	
					for (int i = 0; i < enemigos.size(); i++) {
						context.drawImage(enemigos.get(i).getSpriteEnemy(), enemigos.get(i).getPosX(), enemigos.get(i).getPosY());
					}
	
				}
	
			}.start();
		}//Cierre del metodo
	
		   /**
		    * M�todo colision lansa un boolean dependiendo las posiciones de los personajes.
			* @param temporal recibe un enemigo para verificar su posicion.
			*/
		public boolean colision(Enemy temporal) {
	
			float x = jugador1.getPosX();
			float y = jugador1.getPosY();
			float Mx = jugador1.getRangeX();
			float My = jugador1.getRangeY();
			int m = (int) temporal.getPosX();
			int n = temporal.getPosY();
			int Mm = temporal.getRangeX();
			int Mn = temporal.getRangeY();
	
			if((x<=m&&m<=Mx&&y<=n&&n<=My)) {
				return true;
			}
			if((x<=m&&m<=Mx&&y<=Mn&&Mn<=My)) {
				return true;
			}
			if((x<=Mm&&Mm<=Mx&&y<=Mn&&Mn<=My)) {
				return true;
			}
			if((x<=Mm&&Mm<=Mx&&y<=n&&n<=My)) {
				return true;
			}
			return false;
		}//Cierre del metodo
	
		   /**
		    * M�todo win lanza un boolean si el jugador esta en la posicion de llegada.
			* @param temporal recibe la llegada para compararla con la posicion del jugador.
			* @param jugador recibe al jugador actual para verificar si gano o no.
			*/
		
		public boolean win(Llegada temporal, Player jugador) {
	
			float x = jugador.getPosX();
			float y = jugador.getPosY();
			float Mx = jugador.getRangeX();
			float My = jugador.getRangeY();
			int m =  (int) temporal.getPosX();
			int n =  (int) temporal.getPosY();
			int Mm =  (int) temporal.getRangeX();
			int Mn =  (int) temporal.getRangeY();
	
			if((x<=m&&m<=Mx&&y<=n&&n<=My)) {
				return true;
			}
			if((x<=m&&m<=Mx&&y<=Mn&&Mn<=My)) {
				return true;
			}
			if((x<=Mm&&Mm<=Mx&&y<=Mn&&Mn<=My)) {
				return true;
			}
			if((x<=Mm&&Mm<=Mx&&y<=n&&n<=My)) {
				return true;
			}
			return false;
		}//Cierre del metodo
	
	
		   /**
		    * M�todo handle registra las acciones durante la ejecucion del programa.
			*/
	
		@Override
		public void handle(KeyEvent event) {
	
	
			if(event.getCode().equals(KeyCode.UP)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				jugador1.moverArriba();
			}else if(event.getCode().equals(KeyCode.RIGHT)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				jugador1.moverDerecha();
			}else if(event.getCode().equals(KeyCode.LEFT)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				jugador1.moverIzquierda();
			}else if(event.getCode().equals(KeyCode.DOWN)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				jugador1.moverAbajo();
	
			}
		}//Cierre del metodo.
	}

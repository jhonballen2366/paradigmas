package Lights;	
	import javafx.scene.image.Image;
	
	/**
	 * Esta clase contiene todos los atributos y acciones de la meta, para ser llamados desde la principal.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez
	 * @version: 25/05/2019
	 */
	
	//Campos de la clase
	public class Llegada {
	
		private Image llegadaImage;
		private float posX;
		private float posY;
		private float rangeX;
		private float rangeY;
	
		/**
		 * Constructor para la clase de Llegada que contiene todo sobre la linea de meta.
		 */
		
		public Llegada() {
			this.llegadaImage = new Image ("Lights/Imagenes/LlegadaMago.png",150,150,true,false);
			this.posX = 650;
			this.posY = 450;
			this.rangeX = posX +75;
			this.rangeY = posY +75;
	
		}//Cierre del constructor.
	
	
		//Getters and setters
		public float getRangeX() {
			return posX+150;
		}
	
	
		public void setRangeX(float rangeX) {
			this.rangeX = rangeX;
		}
	
	
		public float getRangeY() {
			return posY+150;
		}
	
	
		public void setRangeY(float rangeY) {
			this.rangeY = rangeY;
		}
	
	
		public Image getLlegadaImage() {
			return llegadaImage;
		}
	
	
		public void setLlegadaImage(Image llegadaImage) {
			this.llegadaImage = llegadaImage;
		}
	
	
		public float getPosX() {
			return posX;
		}
	
	
		public void setPosX(float posX) {
			this.posX = posX;
		}
	
	
		public float getPosY() {
			return posY;
		}
	
	
		public void setPosY(float posY) {
			this.posY = posY;
		}//Cierre de getters and setters
	
	}//Cierre de la clase

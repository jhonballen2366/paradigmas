	
package Lights;	
import javafx.application.Application;
	import javafx.scene.Group;
	import javafx.stage.Stage;
	
	/**
	 * Esta clase es la principal, que se encarga de ejecutar todo el programa.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez
	 * @version: 25/05/2019
	 */
	
	//Campos de la clase
	public class LigthOff extends Application {
	
	
	
	    /**
		 * M�todo start para iniciar el programa.
	     */
		
		@Override
		public void start(Stage mainStage) throws Exception {
	
			mainStage.setTitle("LIGHT OFF");
	
			Tablero tablero1 = new Tablero(new Group(), 800, 600,mainStage);
			mainStage.setScene(tablero1);
			mainStage.setTitle("�Ligth Off!");
			tablero1.runLevel();
			mainStage.show();
	
	
		}//Cierre del metodo
	
	
	
	

	    /**
		 * M�todo main para iniciar el programa
	     */
		public static void main(String[] args) {
			launch(args);
		}//Cierre del metodo
	
	
	} //Cierre de la clase
	

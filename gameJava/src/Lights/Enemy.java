package Lights;	
import javafx.scene.image.Image;
	
	/**
	 * Esta clase contiene todos los atributos y acciones de los enemigos, para ser llamados desde la principal.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez
	 * @version: 25/05/2019
	 */
	
	//Campos de la clase
	public class Enemy {
	
		private Image spriteEnemy;
		private int posX;
		private int posY;
		private int rangeX;
		private int rangeY;
		private Image spriteInv;
	
	
		/**
		 * Constructor para la clase de Enemy de los enemigos del juego.
		 */
		public Enemy() {
	
			this.spriteEnemy = new Image ("Lights/Imagenes/EnemigoFinal.png",80,80,true,false);
			this.spriteInv = new Image ("Lights/Imagenes/Enemigoinv.png",80,80,true,false);
			this.posX = randomPosX();
			this.posY = randomPosY();
			this.rangeX = posX + 80;
			this.rangeY = posY + 80;
	
			if (posX<=200&&posY<=200) {
				this.posX = randomPosX();
				this.posY = randomPosY();
			}
		}//Cierre del constructor
	
	
		/**
		 * M�todo randomPosX genera un numero aleatorio para el origen de los enemigos..
		 */
	
		private int randomPosX () {
			int x=3;
			if (x%2!=0) {
				x=(int)(Math.random()*600)+20;
			}
			return x;
	
		}//Cierre del metodo
	
	
		//Getters and setters
		public Image getSpriteInv() {
			return spriteInv;
		}
	
	
		public void setSpriteInv(Image spriteInv) {
			this.spriteInv = spriteInv;
		}
	
	
		public int getRangeX() {
			return posX+40;
		}
	
	
		public void setRangeX(int rangeX) {
			this.rangeX = rangeX;
		}
	
	
		public int getRangeY() {
			return posY+40;
		}
	
		private int randomPosY () {
			return (int)(Math.random()*520)+10;
		}
	
	
	
		public Image getSpriteEnemy() {
			return spriteEnemy;
		}
	
	
		public void setSpriteEnemy(Image spriteEnemy) {
			this.spriteEnemy = spriteEnemy;
		}
	
	
		public float getPosX() {
			return posX;
		}
	
	
		public void setPosX(int posX) {
			this.posX = posX;
		}
	
	
		public int getPosY() {
			return posY;
		}
	
	
		public void setPosY(int posY) {
			this.posY = posY;
		}
	
		public void setRangeY(int rangeY) {
			this.rangeY = rangeY;
		}//Cierre de los getters and setters
	
	}//Cierre de la clase.

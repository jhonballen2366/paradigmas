package Lights;	
	import javafx.scene.image.Image;
	
	/**
	 * Esta clase contiene todos los atributos y acciones del jugador, para ser llamados desde la principal.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez
	 * @version: 25/05/2019
	 */
	
	//Campos de la clase 
	public class Player {
	
		private Image spritePlayer;
		private float posX;
		private float posY;
		private float rangeX;
		private float rangeY;
	
		
		/**
		 * Constructor para la clase de Player que contiene todo sobre el jugador principal.
		 */
	
		public Player() {
	
			this.spritePlayer = new Image ("Lights/Imagenes/PlayerMagoFinal.png",80,80,true,false);
			this.posX = 50;
			this.posY = 50;
			this.rangeX = posX + 40;	
			this.rangeY = posY + 40;
		}//Cierre del constructor
	
	
		/**
		 * M�todo moverArriba le resta a la posicion en Y para mover el jugador hacia arriba.
		 */
		
		public void moverArriba () {
			
			if (this.posY>=0) {
				posY= posY-20;
			} 
			this.rangeX = posX + 80;	
			this.rangeY = posY + 80;
		}//Cierre del metodo
	
		/**
		 * M�todo moverAajo le suma a la posicion en Y para mover el jugador hacia arriba.
		 */
		
		public void moverAbajo () {
	
			if (this.posY<=480)
			{		posY=posY+20;}
			this.rangeX = posX + 80;	
			this.rangeY = posY + 80;
		}//Cierre del metodo
	
	
		/**
		 * M�todo moverIzquierda le resta a la posicion en X para mover el jugador hacia la izquierda.
		 */
		
		public void moverIzquierda () {
	
			if (posX>=0) {
				posX= posX-20;}
			this.rangeX = posX + 80;	
			this.rangeY = posY + 80;
		}//Cierre del metodo
	
		/**
		 * M�todo moverDerecha le suma a la posicion en X para mover el jugador hacia la derecha.
		 */
	
		public void moverDerecha () {
			if (posX <=720) {
				posX= posX+20;}
			this.rangeX = posX + 80;	
			this.rangeY = posY + 80;
		}//Cierre del metodo
	
	
		//Getters and setters
		public Image getSpritePlayer() {
			return spritePlayer;
		}
	
	
		public void setSpritePlayer(Image spritePlayer) {
			this.spritePlayer = spritePlayer;
		}
	
		public float getRangeX() {
			return posX+40;
		}
	
	
		public void setRangeX(float rangeX) {
			this.rangeX = rangeX;
		}
	
	
		public float getRangeY() {
			return posY+40;
	
		}
		public void setRangeY(float rangeY) {
			this.rangeY = rangeY;
		}
	
		public float getPosX() {
			return posX;
		}
	
	
		public void setPosX(float posX) {
			this.posX = posX;
		}
	
	
		public float getPosY() {
			return posY;
		}
	
	
		public void setPosY(float posY) {
			this.posY = posY;
		}//Cierre de getters and getters
	
	
	}//Cierre de la clase.

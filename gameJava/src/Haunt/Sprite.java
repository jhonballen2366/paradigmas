package Haunt;
import javafx.scene.image.Image;
import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;

import javafx.geometry.Rectangle2D;

public class Sprite {
	private Image image;
	private double positionX;
	private double positionY;
	private double velocityX;
	private double velocityY;
	private double width;
	private double height;

	public Sprite() {
		positionX = 0;
		positionY = 0;
		velocityX = 0;
		velocityY = 0;
	}

	public void setImage(Image i) {
		image = i;
		width = i.getWidth();
		height = i.getHeight();
	}

	public void setImage(String filename,int w,int h) {
		Image i = new Image(filename,w,h,false,false);
		setImage(i);
	}

	public void setPosition(double x, double y) {
		positionX = x;
		positionY = y;
	}
	public void update(double time) {
		double auxX=positionX;
		double auxY=positionY;
		positionX += velocityX * time;
		positionY += velocityY * time;
		if(positionX>970||positionX<-60) {
			positionX =auxX;
		}
		if(positionY>773||positionY<-46) {
			positionY =auxY;
		}
	}
	public void velocidadPersonaje(ArrayList<String> input) {
		if (input.contains("LEFT"))
			addVelocity(-10, 0);
		if (input.contains("RIGHT"))
			addVelocity(10, 0);
		if (input.contains("UP"))
			addVelocity(0, -10);
		if (input.contains("DOWN"))
			addVelocity(0, 10);
	}
	public void setVelocity(double x, double y) {
		velocityX = x;
		velocityY = y;
	}

	public void addVelocity(double x, double y) {
		velocityX += x;
		velocityY += y;
	}
	public void render(GraphicsContext gc) {
		gc.drawImage(image, positionX, positionY);
	}

	public Rectangle2D getHitBox() {
		return new Rectangle2D(positionX-25, positionY-25, width-150, height-150);
	}

	public boolean intersects(Sprite s) {
		return s.getHitBox().intersects(this.getHitBox());
	}

	public double getPositionX() {
		return positionX;
	}

	public void setPositionX(double positionX) {
		this.positionX = positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public void setPositionY(double positionY) {
		this.positionY = positionY;
	}

	public double getVelocityX() {
		return velocityX;
	}

	public void setVelocityX(double velocityX) {
		this.velocityX = velocityX;
	}

	public double getVelocityY() {
		return velocityY;
	}

	public void setVelocityY(double velocityY) {
		this.velocityY = velocityY;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public Image getImage() {
		return image;
	}

	
	

}
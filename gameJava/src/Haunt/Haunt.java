package Haunt;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import MainMenu.Administrator;

public class Haunt extends Application implements EventHandler<KeyEvent> {
	private double UDAPTED = 1000000000.0;
	private ArrayList<String> input;
	private ArrayList<Sprite> moneybagList;
	private Group root;
	private Scene scene;
	private Canvas canvas;
	private GraphicsContext gc;
	private Sprite personaje;
	private double lastNanoTime;
	private int score;
	private ImageView backGround;
	private AnimationTimer at;
	private boolean win, check;
	private Stage stage;
	private int vidasGeneral;
	private int dificultad;
	private int scoreGeneral;
	public Haunt() {
		leerDatos();
		input = new ArrayList<String>();
		moneybagList = new ArrayList<Sprite>();
		root = new Group();
		scene = new Scene(root, 1200, 950);
		root.setStyle("");
		canvas = new Canvas(1200, 950);
		gc = canvas.getGraphicsContext2D();
		gc.setFill(Color.BLACK);
		personaje = new Sprite();
		personaje.setImage("Haunt/maletin.png", 300, 300);
		personaje.setPosition(36, 63);
		lastNanoTime = System.nanoTime();
		score = 0;
		check = false;
		backGround = new ImageView(new Image(
				"Haunt/FondoDinero.png", 1200, 950, false, false));

	}
	public void leerDatos() {
		Scanner sc;
		try {
			sc = new Scanner(new File("datos.txt"));
			while(sc.hasNext())
			{
				System.out.println("lklk");
				vidasGeneral=sc.nextInt();
				dificultad=sc.nextInt();
				scoreGeneral=sc.nextInt();
			}
			System.out.println("hol");
		} catch (Exception e) {	
			System.out.println("ERROR");
		}
	}	

	public void ingresar(int x,int y, int z){
		FileWriter escritorArchivo;
		try {
			System.out.println("adhsad");
			escritorArchivo = new FileWriter("datos.txt", true);
			escritorArchivo.append((x+1) +" "+y+" "+z);
			
			escritorArchivo.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void animation() {
		Timeline lineaTiempo = new Timeline();
		Timeline lineaSecundaria = new Timeline();
		Timeline tiempoJuego = new Timeline();

		KeyFrame keyPrimario = new KeyFrame(new Duration(1), (event) -> {
			ImageView cero = new ImageView(
					new Image("Haunt/FondoCero.png", 1200, 950,
							false, false));
			root.getChildren().add(cero);
			lineaSecundaria.play();

		});
		KeyFrame keySecundario = new KeyFrame(Duration.seconds(2), (event) -> {
			game();
			tiempoJuego.play();

		});
		KeyFrame keyTiempo = new KeyFrame(Duration.seconds(10), (event) -> {
			if (!(win == true)) {
				perder();
				check = true;
			}

		});
		tiempoJuego.getKeyFrames().add(keyTiempo);
		lineaTiempo.getKeyFrames().add(keyPrimario);
		lineaSecundaria.getKeyFrames().add(keySecundario);
		lineaTiempo.play();

	}

	@Override
	public void handle(KeyEvent e) {
		String code = e.getCode().toString();
		if (check == true) {
			e.consume();
		} else {
			if (!input.contains(code)) {
				input.add(code);
			}
		}

	}

	@Override
	public void start(Stage app) {
		root.setStyle("-fx-background-color: black");
		app.setTitle("app");
		app.setScene(scene);
		app.show();
		stage= app;
		animation();
	}

	public void game() {
		root.getChildren().add(backGround);
		root.getChildren().add(canvas);
		scene.setOnKeyPressed(this);
		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent e) {
				if (check == true) {
					e.consume();
				} else {
					String code = e.getCode().toString();
					input.remove(code);
				}

			}
		});
		crearDinero();
		at = new AnimationTimer() {

			@Override
			public void handle(long now) {
				render();
				double elapsedTime = (now - lastNanoTime) / UDAPTED;
				lastNanoTime = now;
				personaje.velocidadPersonaje(input);
				personaje.update(elapsedTime);
				ArrayList<Sprite> aux = new ArrayList<Sprite>(moneybagList);
				for (Sprite sprite : aux) {
					if (personaje.intersects(sprite)) {
						moneybagList.remove(sprite);
						score += +1;
					}
				}
				if (score >= 10) {
					at.stop();
					win = true;
					ganar();
				}
			}

		};
		at.start();
	} 

	public void perder() {
		gc.clearRect(0, 0, 1200, 950);
		root.getChildren().clear();
		backGround.setImage(new Image("Haunt/GameOver2.png", 1200,
				950, false, false));
		root.getChildren().add(backGround);

	}

	public void ganar() {
		ingresar(vidasGeneral+1,dificultad,scoreGeneral);
		 		
		gc.clearRect(0, 0, 1200, 950);
		root.getChildren().clear();
		backGround.setImage(new Image("Haunt/YouWin.png", 1200,
				950, false, false));
		root.getChildren().add(backGround);
		Administrator admin= new Administrator(); 
		try {
   		  admin.iniciateGame((int)(Math.random()*admin.getGames().size()), stage);
   		  admin.setScore(admin.getScore()+1);
				} catch (Exception e) {
			
			e.printStackTrace();
		}	
	}

	public void crearDinero() {
		for (int i = 0; i < 10; i++) {
			Sprite moneybag = new Sprite();
			moneybag.setImage("Haunt/bolsa.png", 250, 250);
			double px = 1000 * Math.random() + 10;
			double py = 700 * Math.random() + 10;
			moneybag.setPosition(px, py);
			moneybagList.add(moneybag);
		}
	}

	public void render() {
		gc.clearRect(0, 0, 1200, 950);
		personaje.render(gc);
		for (Sprite moneybag : moneybagList)
			moneybag.render(gc);
	}

	public static void main(String[] args) {
		launch(args);
	}
}
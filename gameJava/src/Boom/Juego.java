package Boom;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import MainMenu.Administrator;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;

public class Juego extends Application implements EventHandler<KeyEvent> {

	private GridPane root;
	private Scene scene;
	private MostrarFlechas up;
	private int pos;
	private Image imagenWinLose;
	private boolean check, win;
	private ImageView imagenWinLoseView;
	private Audio audio;
	private Stage mainStage;
	private int vidasGeneral;
	private int dificultad;
	private int scoreGeneral;

	public Juego() {
		audio = new Audio();
		check = false;
		root = new GridPane();
		// 1980,1010
		scene = new Scene(root, 1200, 950);
		up = new MostrarFlechas();

	}

	@Override
	public void start(Stage primaryStage) {
		mainStage=primaryStage;
	
		audio.getSonidoCero().play();
		root.setVgap(0);
		root.setAlignment(Pos.TOP_LEFT);
		root.setStyle("-fx-background-color: black");
		primaryStage.setScene(scene);
		primaryStage.show();
		long mTime = System.currentTimeMillis(); 
		long end = mTime + 12*1000; // 5 seconds 

		animation(primaryStage,end);

	}

	public void animation(Stage primaryStage,long end) {
//		if(System.currentTimeMillis()>end&&helper==true)
//		{
//		 lose();
//		}
		Timeline lineaTiempo = new Timeline();
		Timeline lineaSecundaria = new Timeline();
		Timeline tiempoJuego = new Timeline();

		KeyFrame keyPrimario = new KeyFrame(new Duration(2000), (event) -> {

			BackgroundImage myBI = new BackgroundImage(new Image("Boom/FondoCero.png", 1200, 950, false, true),
					BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
					BackgroundSize.DEFAULT);
			root.setBackground(new Background(myBI));
			lineaSecundaria.play();

		});

		KeyFrame keySecundario = new KeyFrame(Duration.seconds(2), (event) -> {
			root.getChildren().clear();
			game(primaryStage);
			tiempoJuego.play();

		});
		KeyFrame keyTiempo = new KeyFrame(Duration.seconds(8), (event) -> {
			check = true;
			if (win == false) {
				lose();
			}

		});
		tiempoJuego.getKeyFrames().add(keyTiempo);
		lineaTiempo.getKeyFrames().add(keyPrimario);
		lineaSecundaria.getKeyFrames().add(keySecundario);
		lineaTiempo.play();

	}

	@Override
	public void handle(KeyEvent game) {
		String key = game.getCode().toString();
		if (check == true) {
			game.consume();

		} else {
			if (pos < up.getListaFlechas().size()) {
				if (key.equals("LEFT")) {
					if (up.getListaFlechas().get(pos).getDirection() == 1) {

						Image aux = new Image("Boom/flechaVerdeI.png");
						resetShowView(aux);

					} else {

						Image aux = new Image("Boom/flechaRojaI.png");
						up.getListaFlechas().get(pos).getImageViewDeafult().setImage(aux);
						resetShowView(aux);
						lose();
						check = true;

					}
				}
				if (key.equals("RIGHT")) {
					if (up.getListaFlechas().get(pos).getDirection() == 2) {

						Image aux = new Image("Boom/flechaVerdeD.png");
						resetShowView(aux);

					} else {

						Image aux = new Image("Boom/flechaRojaD.png");
						resetShowView(aux);
						lose();
						check = true;
					}
				}
				pos++;
			}

		}
		int aux = pos;
		aux++;
		if (aux > up.getListaFlechas().size()) {
			win();
		}
	}

	public void game(Stage primaryStage) {
		root.add(up, 0, 1);
		primaryStage.setScene(scene);
		primaryStage.show();
		BackgroundImage myBI = new BackgroundImage(new Image("Boom/FondoUno.png", 1200, 950, false, true),
				BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				BackgroundSize.DEFAULT);
		root.setBackground(new Background(myBI));
		scene.setOnKeyPressed(this);
		
	}

	public void lose() {
		imagenWinLose = new Image("Boom/GameOver2.png");
		imagenWinLoseView = new ImageView(imagenWinLose);
		imagenWinLoseView.setFitWidth(1200);
		imagenWinLoseView.setFitHeight(950);
		root.getChildren().clear();
		root.add(imagenWinLoseView, 0, 0);
		root.setVgap(0);
		root.setAlignment(Pos.CENTER);

	}

	public void win() {
		Administrator admin = new Administrator();
		audio.getSonidoCero().stop();
		audio.getSonidoWin().play();

		win = true;
		imagenWinLose = new Image("Boom/YouWin2.png");

		imagenWinLoseView = new ImageView(imagenWinLose);
		imagenWinLoseView.setFitWidth(1200);
		imagenWinLoseView.setFitHeight(950);

		root.getChildren().clear();
		root.add(imagenWinLoseView, 0, 0);
		root.setVgap(0);
		root.setAlignment(Pos.CENTER);
		check = true;
		 ingresar(vidasGeneral+1,dificultad,scoreGeneral);
		 try {
   		  admin.iniciateGame((int)(Math.random()*admin.getGames().size()), mainStage);
   		  admin.setScore(admin.getScore()+1);
				} catch (Exception e) {
			
			e.printStackTrace();
		}

	}

	public void resetShowView(Image aux) {
		up.getListaFlechas().get(pos).getImageViewDeafult().setImage(aux);
		root.getChildren().clear();
		root.add(up, 0, 1);
		root.setVgap(0);
		root.setAlignment(Pos.TOP_LEFT);

	}

	public static void main(String[] args) {
		launch(args);
	}
	public void leerDatos() {
		Scanner sc;
		try {
			sc = new Scanner(new File("datos.txt"));
			while(sc.hasNext())
			{
				System.out.println("lklk");
				vidasGeneral=sc.nextInt();
				dificultad=sc.nextInt();
				scoreGeneral=sc.nextInt();
			}
			System.out.println("hol");
		} catch (Exception e) {	
			System.out.println("ERROR");
		}
	}	

	public void ingresar(int x,int y, int z){
		FileWriter escritorArchivo;
		try {
			System.out.println("adhsad");
			escritorArchivo = new FileWriter("datos.txt", true);
			escritorArchivo.append((x+1) +" "+y+" "+z);
			
			escritorArchivo.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

package Boom;

import javafx.scene.media.AudioClip;

public class Audio {

	private AudioClip sonidoCero,sonidoWin;

	public Audio() {

		sonidoCero = new AudioClip(this.getClass().getResource("bombermanSound- Introduction.wav").toString());
		sonidoWin= new AudioClip(this.getClass().getResource("Victory - Super Mario RPG.mp3").toString());
	}

	public AudioClip getSonidoCero() {
		return sonidoCero;
	}

	public void setSonidoCero(AudioClip sonidoCero) {
		this.sonidoCero = sonidoCero;
	}

	public AudioClip getSonidoWin() {
		return sonidoWin;
	}

	public void setSonidoWin(AudioClip sonidoWin) {
		this.sonidoWin = sonidoWin;
	}
	

}

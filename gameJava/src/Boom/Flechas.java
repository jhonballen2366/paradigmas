package Boom;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public class Flechas extends GridPane {
	private int direction;
	private Image imageDefault;
	private ImageView imageViewDeafult;

	public Flechas() {
		chooseDirectionDefault();

	}

	public void chooseDirectionDefault() {

		int x = (int) (Math.random() * 4 + 1);
		if (x == 1) {
			direction = 1;
			imageDefault = new Image("Boom/FlechaGrisI.png");
			imageViewDeafult = new ImageView(imageDefault);

		} else if (x == 2) {
			direction = 2;
			imageDefault = new Image("Boom/FlechaGrisD.png");
			imageViewDeafult = new ImageView(imageDefault);
		} else if (x == 3) {
			direction = 1;
			imageDefault = new Image("Boom/FlechaGrisDReves.png");
			imageViewDeafult = new ImageView(imageDefault);
		} else {
			direction = 2;
			imageDefault = new Image("Boom/FlechaGrisIReves.png");
			imageViewDeafult = new ImageView(imageDefault);
		}
		imageViewDeafult.setFitWidth(85);
		imageViewDeafult.setFitHeight(90);
		this.add(imageViewDeafult, 0, 0);
		this.setAlignment(Pos.CENTER);

	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public Image getImageDefault() {
		return imageDefault;
	}

	public void setImageDefault(Image imageDefault) {
		this.imageDefault = imageDefault;
	}

	public ImageView getImageViewDeafult() {
		return imageViewDeafult;
	}

	public void setImageViewDeafult(ImageView imageViewDeafult) {
		this.imageViewDeafult = imageViewDeafult;
	}

}

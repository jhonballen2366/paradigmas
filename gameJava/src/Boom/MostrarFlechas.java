package Boom;

import java.util.ArrayList;

import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;

public class MostrarFlechas extends GridPane {

	private ArrayList<Flechas> listaFlechas;
	private Flechas flecha;

	public MostrarFlechas() {
		listaFlechas = new ArrayList<Flechas>();
		llenarLista();
		addGrid();
	}

	public void llenarLista() {
		int cantFlechas = (int) (Math.random() * 14 + 1);
		for (int i = 0; i < cantFlechas; i++) {
			flecha = new Flechas();
			listaFlechas.add(flecha);
		}
	}

	public void addGrid() {
		for (int i = 0; i < listaFlechas.size(); i++) {
			this.add(listaFlechas.get(i).getImageViewDeafult(), i, 0);
		}
		this.setHgap(0);
		this.setVgap(0);
		this.setStyle("-fx-background-color: black");

	}

	public ArrayList<Flechas> getListaFlechas() {
		return listaFlechas;
	}

	public void setListaFlechas(ArrayList<Flechas> listaFlechas) {
		this.listaFlechas = listaFlechas;
	}

	public Flechas getFlecha() {
		return flecha;
	}

	public void setFlecha(Flechas flecha) {
		this.flecha = flecha;
	}

}

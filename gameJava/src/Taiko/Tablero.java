package Taiko;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import MainMenu.Administrator;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Tablero extends Scene implements EventHandler<KeyEvent>{

	private Canvas canvasNivel;
	private Image background;
	private Image endBackground;
	private Image fondo;
	private GraphicsContext contexto;
	private Objetivo objetivo; 
	private ArrayList<Tambor> tambores;
	private ArrayList<ImageView> vidas;

	private Video video;
	private HBox puntos;
	private HBox guardarVidas;

	private HBox datos;
	private Label puntajeTexto;
	private Label mostrarPuntaje;
	private int puntaje;
	private AnchorPane orden;
	private Group copiaScena;
	private Stage mainStage;
	private int vidasGeneral;
	private int dificultad;
	private int scoreGeneral;

	private boolean helper;
	public Tablero(Group root, double width, double height,Stage mainStage) {
		super(root);
		leerDatos();
		this.mainStage= mainStage;
		orden = new AnchorPane();
		orden.setMinSize(1024, 650);
		datos = new HBox();
		datos.setSpacing(40);
		puntos = new HBox();
		puntajeTexto=new Label("PUNTAJE: ");
		puntajeTexto.setStyle("-fx-base: rgb(153,0,23);");
		mostrarPuntaje=new Label(""+puntaje);
		mostrarPuntaje.setStyle("-fx-base: rgb(153,0,23);");
		puntaje=0;
		helper= true;
		guardarVidas=new HBox();
		guardarVidas.setSpacing(40);
		vidas=new ArrayList<ImageView>();
		vidas.add(new ImageView(new Image("Taiko/Imagenes/NotaMusical.png",30,30,true,false)));
		vidas.add(new ImageView(new Image("Taiko/Imagenes/NotaMusical.png",30,30,true,false)));
		vidas.add(new ImageView(new Image("Taiko/Imagenes/NotaMusical.png",30,30,true,false)));
		guardarVidas.getChildren().addAll(vidas.get(0),vidas.get(1),vidas.get(2));
		puntos.getChildren().addAll(puntajeTexto,mostrarPuntaje);
		video = new Video();
		orden.getChildren().add(video.getVideo());
		AnchorPane.setBottomAnchor(video.getVideo(), 0.0);
		AnchorPane.setLeftAnchor(video.getVideo(), 100.0);
		datos.getChildren().addAll(puntos,guardarVidas);
		datos.setSpacing(700);
		canvasNivel=new Canvas(width,height);
		objetivo=new Objetivo();
		tambores=new ArrayList<Tambor>();
		background=new Image("Taiko/Imagenes/BACKGROUND.jpg",1024,650,false,false);
		endBackground=new Image("Taiko/Imagenes/endGame.png",1024,650,false,false);
		fondo=new Image("Taiko/Imagenes/Fondo.png",1024,100,false,false);

		root.getChildren().addAll(canvasNivel,datos,orden);
		copiaScena=root;
		contexto=canvasNivel.getGraphicsContext2D();
		this.setOnKeyPressed(this);


	}
	public void leerDatos() {
		Scanner sc;
		try {
			sc = new Scanner(new File("datos.txt"));
			while(sc.hasNext())
			{
				System.out.println("lklk");
				vidasGeneral=sc.nextInt();
				dificultad=sc.nextInt();
				scoreGeneral=sc.nextInt();
			}
			System.out.println("hol");
		} catch (Exception e) {	
			System.out.println("ERROR");
		}
	}	

	public void ingresar(int x,int y, int z){
		FileWriter escritorArchivo;
		try {
			System.out.println("adhsad");
			escritorArchivo = new FileWriter("datos.txt", true);
			escritorArchivo.append((x+1) +" "+y+" "+z);

			escritorArchivo.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void runNivel() {

		Administrator admin = new Administrator();
		long mTime = System.currentTimeMillis(); 
		long end = mTime + 15*1000; // 10 seconds 

		new AnimationTimer() {

			@Override
			public void handle(long arg0) {

				if(System.currentTimeMillis()>end&&helper==true)
				{
					helper=false;
					try {
						ingresar(vidasGeneral+1,dificultad,scoreGeneral);
						video.pararVideo();
						admin.iniciateGame((int)(Math.random()*admin.getGames().size()), mainStage);
						admin.setScore(admin.getScore()+1);
					} catch (Exception e) {

						e.printStackTrace();
					}
				}
				contexto.drawImage(background, 0, 0);
				contexto.drawImage(fondo, 0, 45);

				contexto.drawImage(objetivo.getImagenObjetivo(),objetivo.getPosObjetivoX(),objetivo.getPosObjtivoY());

				actualizar();
				crearTambores();
				eliminarTambores();
				for (int i=0;i<tambores.size();i++) {
					contexto.drawImage(tambores.get(i).getImagenTambor(),tambores.get(i).getPosTamborX(),tambores.get(i).getPosTamborY());	
					tambores.get(i).movimiento();
				}
				/**
				 * CONDICION OARA FINALIZAR EL JUEGO, QUEDARSE CON VIDAS.SIZE==0;
				 */
				if(vidas.size()==0) {
					copiaScena.getChildren().clear();
					video.pararVideo();
					copiaScena.getChildren().addAll(canvasNivel);
					contexto.drawImage(endBackground, 0, 0);

				}
			}

		}.start();
	}
	@Override
	public void handle(KeyEvent event) {
		try {
			if(event.getCode().equals(KeyCode.A)&& event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				if(tambores.get(0).getCategoria() %2==0) {
					if(objetivo.colisionExacta(tambores.get(0).getPosTamborX(),tambores.get(0).getPosTamborY(),tambores.get(0).getRangoX(),tambores.get(0).getRanyoY())) {
						puntaje=puntaje+10;
						tambores.remove(0);
						return; 
					}else if(objetivo.colisionInexacta(tambores.get(0).getPosTamborX(),tambores.get(0).getPosTamborY(),tambores.get(0).getRangoX(),tambores.get(0).getRanyoY())) {
						puntaje=puntaje+5;
						tambores.remove(0);
						return;
					}

					if(vidas.size()!=0) {
						vidas.remove(0);
						actualizarHBox();
					}
					tambores.remove(0);	

				}else {
					if(vidas.size()!=0) {
						vidas.remove(0);
						actualizarHBox();
					}
					tambores.remove(0);	
				}
			}
			if(event.getCode().equals(KeyCode.D) && event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				if(tambores.get(0).getCategoria() %2!=0) {
					if(objetivo.colisionExacta(tambores.get(0).getPosTamborX(),tambores.get(0).getPosTamborY(),tambores.get(0).getRangoX(),tambores.get(0).getRanyoY())) {
						puntaje=puntaje+10;
						tambores.remove(0);
						return;
					}
					else if(objetivo.colisionInexacta(tambores.get(0).getPosTamborX(),tambores.get(0).getPosTamborY(),tambores.get(0).getRangoX(),tambores.get(0).getRanyoY())) {
						puntaje=puntaje+5;
						tambores.remove(0);
						return;
					}
					if(vidas.size()!=0) {
						vidas.remove(0);
						actualizarHBox();
					}
					tambores.remove(0);	
				}else {
					if(vidas.size()!=0) {
						vidas.remove(0);
						actualizarHBox();
					}
					tambores.remove(0);	
				}
			}
		}catch(IndexOutOfBoundsException e) {

		}

	}


	public void actualizarHBox() {
		guardarVidas.getChildren().clear();
		for (int i = 0; i < vidas.size(); i++) {
			guardarVidas.getChildren().add(vidas.get(i));
		}

	}

	public void crearTambores() {

		if(tambores.size()==0) {

			Tambor nuevoTambor=new Tambor();
			tambores.add(nuevoTambor);
		}
		else if(tambores.get(tambores.size()-1).getPosTamborX()>1000 && tambores.get(tambores.size()-1).getPosTamborX()<1009) {
			int aux=(int)(Math.random()*9)+1;
			if(aux<8) {
				Tambor nuevoTambor1=new Tambor();
				tambores.add(nuevoTambor1);
			}	
		}
	}

	public void eliminarTambores() {
		for (int i = 0; i <tambores.size(); i++) {
			if(tambores.get(i).getPosTamborX()<-4) {
				tambores.remove(i);
			}

		}
	}

	public void actualizar() {
		mostrarPuntaje.setText(""+puntaje);
	}


	public Objetivo getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(Objetivo objetivo) {
		this.objetivo = objetivo;
	}

	public ArrayList<Tambor> getTambores() {
		return tambores;
	}

	public void setTambores(ArrayList<Tambor> tambores) {
		this.tambores = tambores;
	}

	public Label getPuntajeTexto() {
		return puntajeTexto;
	}

	public void setPuntajeTexto(Label puntajeTexto) {
		this.puntajeTexto = puntajeTexto;
	}

	public Label getMostrarPuntaje() {
		return mostrarPuntaje;
	}

	public void setMostrarPuntaje(Label mostrarPuntaje) {
		this.mostrarPuntaje = mostrarPuntaje;
	}

	public int getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}









}



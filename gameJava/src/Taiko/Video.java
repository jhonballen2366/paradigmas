package Taiko;
import java.io.File;

import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

public class Video {

	private MediaPlayer mediaPlayer;
	private HBox video;
	private Media temporal;
	
	public Video() {
		
		String path = "src/Taiko/Imagenes/VideoKDA.mp4";  
		  //Instantiating Media class  
        Media media = new Media(new File(path).toURI().toString());  
          
        //Instantiating MediaPlayer class   
        mediaPlayer = new MediaPlayer(media);  
          
        //Instantiating MediaView class   
        MediaView mediaView = new MediaView(mediaPlayer);  
          
       
        //by setting this property to true, the Video will be played   
        mediaPlayer.setAutoPlay(true);  
        
		video = new HBox();
		video.getChildren().add(mediaView);
	}
	
	public void pararVideo() {
		mediaPlayer.pause();
	}

	public HBox getVideo() {
		return video;
	}
	
}

package Taiko;
import javafx.scene.image.Image;

public class Tambor {
	private Image imagenTambor;
	private float posTamborX;
	private float posTamborY;
	
	private int categoria;
	private int velocidad;
	
	public Tambor() {
		this.posTamborX=1110;
		this.posTamborY=55;
		this.velocidad=10;
		categoria=(int)(Math.random()*9)+1;
		if(categoria%2==0) {
			imagenTambor=new Image("Taiko/Imagenes/Tambor1.png",70,70,true,false);
		}
		else {
			imagenTambor=new Image("Taiko/Imagenes/Tambor2.png",70,70,true,false);
		}
		
	}
	public void movimiento() {
		posTamborX-=velocidad;
	}

	public Image getImagenTambor() {
		return imagenTambor;
	}

	

	public float getPosTamborX() {
		return posTamborX;
	}

	

	public float getPosTamborY() {
		return posTamborY;
	}

	

	public float getRangoX() {
		return posTamborX+70;
	}

	

	public float getRanyoY() {
		return posTamborY+70;
	}

	

	public int getCategoria() {
		return categoria;
	}

	
	
	
	
	
	
	
	
	

}

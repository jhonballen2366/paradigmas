package Taiko;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.stage.Stage;

public class Main extends Application{
	
	@Override
	public void start(Stage mainStage) throws Exception {
		mainStage.setTitle("GAME"); 
		Tablero tablero01=new Tablero(new Group(),1024,650,mainStage);
		mainStage.setScene(tablero01);
		tablero01.runNivel();
		mainStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}

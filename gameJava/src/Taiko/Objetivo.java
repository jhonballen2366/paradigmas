package Taiko;


import javafx.scene.image.Image;

public class Objetivo{
	private Image imagenObjetivo;
	private float posObjetivoX;
	private float posObjetivoY;
	private float rangoObjetivoX;
	
	public Objetivo() {
		imagenObjetivo=new Image("Taiko/Imagenes/TamborUsuario.png",90,90,true,false);
		this.posObjetivoX=10;
		this.posObjetivoY=50;
		this.rangoObjetivoX=90;
	}
	
	public boolean colisionInexacta(float x,float y, float Mx, float My) {
		if((x<posObjetivoX)&&(Mx>posObjetivoX)) {
			return true;
		}
		if((x<rangoObjetivoX)&&(Mx>rangoObjetivoX)) {
			return true;
		}
		return false;
	}
	
	public boolean colisionExacta(float x,float y, float Mx, float My) {
		return ((posObjetivoX<=x)&&(x<=rangoObjetivoX)&&(rangoObjetivoX>=Mx));
	}
	
	
	public Image getImagenObjetivo() {
		return imagenObjetivo;
	}

	public void setImagenObjetivo(Image imagenObjetivo) {
		this.imagenObjetivo = imagenObjetivo;
	}

	public float getPosObjetivoX() {
		return posObjetivoX;
	}

	public void setPosObjetivoX(float posObjetivoX) {
		this.posObjetivoX = posObjetivoX;
	}

	public float getPosObjtivoY() {
		return posObjetivoY;
	}

	public void setPosObjtivoY(float posObjtivoY) {
		this.posObjetivoY = posObjtivoY;
	}

	public float getRangoObjetivoX() {
		return posObjetivoX+90;
	}

	public void setRangoObjetivoX(float rangoObjetivoX) {
		this.rangoObjetivoX = rangoObjetivoX;
	}

	public float getRangoObjetivoY() {
		return posObjetivoX+90;
	}

	public void setRangoObjetivoY(float rangoObjetivoY) {
	}
	
	
	

}

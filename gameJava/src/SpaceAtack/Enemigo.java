package SpaceAtack;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Enemigo {
	private int da�o;
	private double posX, posY, velX, velY;
	private double alto, ancho;
	private Image sprite;

	public Enemigo(double x, double y) {
		sprite = new Image("SpaceAtack/asteroide.png", 100, 100, true,
				true);
		posX = x;
		posY = y;
		velX = 0;
		velY = 0;
		ancho = sprite.getWidth();
		alto = sprite.getHeight();
		da�o = (int) ((10 * Math.random()) + 1);
	}

	public void setVelocidad(double x, double y) {
		velX = x;
		velY = y;
	}

	public void actualizar(double time,Personaje p) {
		movimeintos();
		posX += velX * time;
		posY += velY * time;
		
		
	}

	public void movimeintos() {
		int eleccion = (int) (200 * Math.random() + 1);
		 if (eleccion == 4) {
			velY += +100;
		}

	}

	public void chase(Personaje p) {
		int teleport = (int) (600 * Math.random() + 1);
		if (p.getPosX() > posX) {
			setPosicion(posX + 1, posY);
		}
		if (p.getPosX() < posX) {
			setPosicion(posX - 1, posY);
		}
		if (p.getPosY() > posY) {
			setPosicion(posX, posY + 1);
		}
		if (p.getPosY() < posY) {
			setPosicion(posX, posY - 1);
		}
		if (teleport == 3) {
			setPosicion(p.getPosX() + 200, p.getPosY() + 200);
		}
	}

	public void setPosicion(double x, double y) {
		posX = x;
		posY = y;
	}

	public void render(GraphicsContext gc) {
		gc.drawImage(sprite, posX, posY);
	}

	public Rectangle2D HitBox() {
		return new Rectangle2D(posX, posY, ancho-50, alto-50);
	}
	
	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public int getDa�o() {
		return da�o;
	}

	public void setDa�o(int da�o) {
		this.da�o = da�o;
	}

}

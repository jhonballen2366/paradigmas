
package SpaceAtack;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Bala {
	private double posX, posY,velY;
	private double alto, ancho;
	private Image sprite;

	public Bala(double x,double y) {
		posX = x-25;
		posY = y-50;
		velY = 1800;
		sprite = new Image("SpaceAtack/bala.png", 150, 150, true, true);
		alto = sprite.getHeight();
		ancho = sprite.getWidth();

	}
	public void actualizar(double time) {
		double aux2 = posY;
		posY -= velY * time;
		
	}
	public void render(GraphicsContext gc) {
		gc.drawImage(sprite, posX, posY);
	}

	public Rectangle2D HitBox() {
		return new Rectangle2D(posX, posY, ancho, alto);
	}
	public boolean intersects(Enemigo enemigo) {
		return this.HitBox().intersects(enemigo.HitBox());
	}
	public double getPosY() {
		return posY;
	}
	public void setPosY(double posY) {
		this.posY = posY;
	}
	
	

}

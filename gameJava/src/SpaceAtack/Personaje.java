package SpaceAtack;
import java.util.ArrayList;

import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Personaje {
	private double posX, posY, velX, velY;
	private double alto, ancho;
	private Image sprite;

	public Personaje() {
		posX = 0;
		posY = 0;
		velX = 0;
		velY = 0;
		sprite = new Image("SpaceAtack/Nave.png", 120, 120, true, true);
		alto = sprite.getHeight();
		ancho = sprite.getWidth();

	}

	public void setPosicion(double x, double y) {
		posX = x;
		posY = y;
	}

	public void setVelocidad(double x, double y) {
		velX = x;
		velY = y;
	}
	public void actualizar(double time) {
		double aux = posX;
		double aux2 = posY;
		posX += velX * time;
		posY += velY * time;
		if (posX > 1101 || posX < 8) {
			posX = aux;
		}
		if (posY > 950 || posY < 60) {
			posY = aux2;
		}

	}

	public void render(GraphicsContext gc) {
		gc.drawImage(sprite, posX, posY);
	}

	public Rectangle2D HitBox() {
		return new Rectangle2D(posX, posY, ancho, alto);
	}
	public boolean intersects(Enemigo enemigo) {
		return this.HitBox().intersects(enemigo.HitBox());
	}
	
	
	// falta saber si chocaron
	public void inputs(ArrayList<String> inputs) {
		setVelocidad(0, 0);
		if (inputs.contains("LEFT")) {
			velX += -700;
		}
		if (inputs.contains("RIGHT")) {
			velX += +700;
		}

	}
	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}
	

}

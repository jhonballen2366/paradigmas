package SpaceAtack;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import MainMenu.Administrator;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Space extends Application implements EventHandler<KeyEvent> {
	private double CONSTANT = 1000000000.0;
	private Scene scene;
	private Group root;
	private Label test;
	private Canvas canvas;
	private GraphicsContext gc;
	private AnimationTimer at;
	private double nanoTime;
	private Personaje personaje;
	private ArrayList<String> inputs;
	private ArrayList<Enemigo> listaE;
	private GestionadorBalas balas;
	private boolean check, status;
	private ImageView backGround;
	private Stage stage;
	private int vidasGeneral;
	private int dificultad;
	private int scoreGeneral;
	public Space() {
		leerDatos();
		inputs = new ArrayList<>();
		listaE = new ArrayList<>();
		root = new Group();
		test = new Label("Empezar Partida");
		scene = new Scene(root, 1200, 950);
		personaje = new Personaje();
		personaje.setPosicion(382, 680);
		balas = new GestionadorBalas();
		listaE.add(crearEnemigo());
		listaE.add(crearEnemigo());
		listaE.add(crearEnemigo());
		listaE.add(crearEnemigo());
		listaE.add(crearEnemigo());
		backGround = new ImageView(new Image("SpaceAtack/FondoUno.png",
				1200, 950, false, false));
		labels();
	}
	private void main2() {
		canvas = new Canvas(1200, 950);
		gc = canvas.getGraphicsContext2D();
		nanoTime = System.nanoTime();
		root.getChildren().add(backGround);
		root.getChildren().add(canvas);
	} 	
	@Override
	public void handle(KeyEvent key) {
		String aux = key.getCode().toString();
		if (check == true) {
			key.consume();
		} else {
			if (!inputs.contains(aux)) {
				inputs.add(aux);
			}
			if (aux.equals("UP") || aux.equals("SPACE")) {
				balas.add(personaje.getPosX(), personaje.getPosY());
			}
		}

	}
 
	@Override
	public void start(Stage app) throws Exception {
		scene.setRoot(root);
		animation();
		stage=app;
		app.setTitle("Space!");
		app.setScene(scene);
		app.show();

	}

	public void animation() {
		Timeline lineaTiempo = new Timeline();
		Timeline lineaSecundaria = new Timeline();
		Timeline tiempoJuego = new Timeline();

		KeyFrame keyPrimario = new KeyFrame(new Duration(1), (event) -> {
			ImageView cero = new ImageView(new Image(
					"SpaceAtack/FondoCero.png", 1200, 950, false, false));
			root.getChildren().add(cero);
			lineaSecundaria.play();

		});

		KeyFrame keySecundario = new KeyFrame(Duration.seconds(2), (event) -> {
			root.getChildren().clear();
			main2();
			all();
			tiempoJuego.play();

		});
		KeyFrame keyTiempo = new KeyFrame(Duration.seconds(8), (event) -> {
			if (!(status == true)) {
				ganar();
				check = true;
				at.stop();
			}

		});
		tiempoJuego.getKeyFrames().add(keyTiempo);
		lineaTiempo.getKeyFrames().add(keyPrimario);
		lineaSecundaria.getKeyFrames().add(keySecundario);
		lineaTiempo.play();

	}
	public void leerDatos() {
		Scanner sc;
		try {
			sc = new Scanner(new File("datos.txt"));
			while(sc.hasNext())
			{
				System.out.println("lklk");
				vidasGeneral=sc.nextInt();
				dificultad=sc.nextInt();
				scoreGeneral=sc.nextInt();
			}
			System.out.println("hol");
		} catch (Exception e) {	
			System.out.println("ERROR");
		}
	}	

	public void ingresar(int x,int y, int z){
		FileWriter escritorArchivo;
		try {
			System.out.println("adhsad");
			escritorArchivo = new FileWriter("datos.txt", true);
			escritorArchivo.append((x+1) +" "+y+" "+z);
			
			escritorArchivo.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void perder() {
		gc.clearRect(0, 0, 1200, 950);
		root.getChildren().clear();
		backGround.setImage(new Image("SpaceAtack/GameOver2.png", 1200,
				950, false, false));
		root.getChildren().add(backGround);

	}

	public void ganar() {
		ingresar(vidasGeneral+1,dificultad,scoreGeneral);
		gc.clearRect(0, 0, 1200, 950);
		root.getChildren().clear();
		backGround.setImage(new Image("SpaceAtack/YouWin.png", 1200,
				950, false, false));
		root.getChildren().add(backGround);
		Administrator admin= new Administrator(); 
		try {
   		  admin.iniciateGame((int)(Math.random()*admin.getGames().size()), stage);
   		  admin.setScore(admin.getScore()+1);
				} catch (Exception e) {
			
			e.printStackTrace();
		}	
	}

	private void all() {
		scene.setOnKeyPressed(this);
		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent key) {
				if (check == true) {
					key.consume();
				} else {
					String aux = key.getCode().toString();
					inputs.remove(aux);
				}
			}
		});
		at = new AnimationTimer() {

			@Override
			public void handle(long now) {
				render();
				balas.render(gc);
				int x = (int) (12 * Math.random() + 1);
				mostrarenemigos(x);
				double curTime = (now - nanoTime) / CONSTANT;
				nanoTime = now;
				personaje.inputs(inputs);
				personaje.actualizar(curTime);
				balas.actualizar(curTime);
				for (Enemigo enemigo : listaE) {
					enemigo.actualizar(curTime, personaje);
				}
				ArrayList<Enemigo> enemigosAux = new ArrayList<>(listaE);
				ArrayList<Bala> balaAux = new ArrayList<>(balas.getListaB());
				Iterator<Enemigo> enemgiosI = enemigosAux.iterator();
				while (enemgiosI.hasNext()) {
					Enemigo enemigo = enemgiosI.next();
					if (personaje.intersects(enemigo)) {
						listaE.remove(enemigo);
						status = true;

					}
					for (Bala bala : balaAux) {
						if (bala.intersects(enemigo)) {
							balas.getListaB().remove(bala);
							listaE.remove(enemigo);
						}
						if (bala.getPosY() < -20) {
							balas.getListaB().remove(bala);
						}
					}

				}
				for (Enemigo enemigo : enemigosAux) {
					if (enemigo.getPosY() > 680) {
						listaE.remove(enemigo);
						status = true;
					}
				}
				if (status == true) {
					at.stop();
					perder();
				}
			}
		};
		at.start();

	}

	private void mostrarenemigos(int x) {
		if (x == 4) {
			listaE.add(crearEnemigo());
		}
	}

	private void render() {
		gc.clearRect(0, 0, 1200, 950);
		personaje.render(gc);
		for (Enemigo enemigo : listaE) {
			enemigo.render(gc);
		}
	}

	private Enemigo crearEnemigo() {
		int x = (int) (950 * Math.random() + 60);
		int y = (int) (-400 * Math.random());
		Enemigo enemigo = new Enemigo(x, y);
		return enemigo;

	}

	private void labels() {
		test.setFont(new Font("Comics Sans", 72));
		test.setTextFill(Color.web("#00ffff"));
		test.setStyle("-fx-font-style: italic");
		test.setAlignment(Pos.TOP_LEFT);
	}

	public static void main(String[] args) {
		launch(args);
	}

}

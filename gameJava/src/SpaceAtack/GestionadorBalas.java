package SpaceAtack;
import java.util.ArrayList;

import javafx.scene.canvas.GraphicsContext;

public class GestionadorBalas {
	
	private ArrayList<Bala> listaB;
	
	public GestionadorBalas() {
		listaB=new ArrayList<>();
	}
	public void add(double x, double y) {
		Bala bala=new Bala(x, y);
		listaB.add(bala);
	}
	public void render(GraphicsContext gc) {
		for (Bala bala : listaB) {
			bala.render(gc);
		}
	}
	public void actualizar(double time) {
		for (Bala bala : listaB) {
			bala.actualizar(time);
		}
	}
	public ArrayList<Bala> getListaB() {
		return listaB;
	}
	public void setListaB(ArrayList<Bala> listaB) {
		this.listaB = listaB;
	}
	
}

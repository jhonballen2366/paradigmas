package Pez;
	
	import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import MainMenu.Administrator;
import javafx.animation.AnimationTimer;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
	import javafx.scene.Group;
	import javafx.scene.Scene;
	import javafx.scene.canvas.Canvas;
	import javafx.scene.canvas.GraphicsContext;
	import javafx.scene.control.TextField;
	import javafx.scene.image.Image;
	import javafx.scene.input.KeyCode;
	import javafx.scene.input.KeyEvent;
	import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
	import javafx.scene.text.Text;
import javafx.stage.Stage;
	
	/**
	 * Esta clase se encarga de ejecutar cada accion y funcionamiento completo del programa.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez
	 * @version: 25/05/2019
	 */
	
	//Campos de la clase
	public class NivelPrincipal extends Scene implements EventHandler<KeyEvent> {
	
		private Canvas canvasNivel;
		private Image background;
		private GraphicsContext contexto;
		private Jugador player;
		private ArrayList<Enemigo> enemigos;
		private ArrayList<Vida> vidas;
		private ArrayList<Bonus> bonus;
		private boolean helper;
		private VBox scores;
		private TextField score;
		private Text scoreText;
		private Stage mainStage;
		private Media musica;
		private MediaPlayer reproducir;
		private String path;
		
		private static float cantidadPuntos;
		private static int cantidadEnemigos;
		private int vidasGeneral;
		private int dificultad;
		private int scoreGeneral;
		/**
		 * Constructor para la clase del nivel principal.
		 * @param root porque se extiende a Scena
		 * @param width asigna el tama�o a la Scene
		 * @param height asigna el tama�o a la Scene
		 */
	
	
		public NivelPrincipal(Group root, double width, double height,Stage mainStage) {
			super(root);
			this.mainStage=mainStage;
			this.scores = new VBox();
			leerDatos();
			this.bonus = new ArrayList<Bonus>();
			this.vidas= new ArrayList<Vida>();
			crearVidas();
			this.cantidadEnemigos=8;
			this.cantidadPuntos=0;
			this.score =new TextField(Float.toString(cantidadPuntos));
			this.scoreText = new Text ("SCORE: ");
			scoreText.setFill(Color.DARKBLUE);
			scoreText.setStyle("-fx-font: 40 arial;");
			score.setStyle("-fx-font: 40 arial;");
			score.setMaxWidth(200);	
			score.setDisable(true);
			scores.getChildren().addAll(scoreText,score);		
			this.enemigos= new ArrayList<Enemigo>();
			this.canvasNivel = new Canvas(width,height);
			this.background = new Image ("Pez/Imagenes/FondoJuegopez.jpg",width,height,true,false);
			root.getChildren().addAll(canvasNivel,scores);
			this.contexto=canvasNivel.getGraphicsContext2D();
			this.setOnKeyPressed(this);	
			this.path = "src/Pez/Imagenes/Musicaop.mp3";
			this.musica =new Media (new File(path).toURI().toString());  
			this.reproducir = new MediaPlayer (musica);
			this.player = new Jugador ();
			helper=true;
		}//Cierre del constructor
		public void leerDatos() {
			Scanner sc;
			try {
				sc = new Scanner(new File("datos.txt"));
				while(sc.hasNext())
				{
					System.out.println("lklk");
					vidasGeneral=sc.nextInt();
					dificultad=sc.nextInt();
					scoreGeneral=sc.nextInt();
				}
				System.out.println("hol");
			} catch (Exception e) {	
			}
		}	
	
		public void ingresar(int x,int y, int z){
			FileWriter escritorArchivo;
			try {
				System.out.println("adhsad");
				escritorArchivo = new FileWriter("datos.txt", true);
				escritorArchivo.append((x+1) +" "+y+" "+z);
				
				escritorArchivo.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		/**
		 * M�todo runLevel ejecuta el hilo en el que se correra el programa.
		 */
		public void runNivel() {			
			Administrator admin = new Administrator();
			long mTime = System.currentTimeMillis(); 
			long end = mTime + 10*1000; // 5 seconds 
			new AnimationTimer() {
				@Override
				public void handle(long arg0) {
					if(System.currentTimeMillis()>end&&helper==true)
					{
						helper=false;
						 try {
							 ingresar(vidasGeneral+1,dificultad,scoreGeneral);
							 admin.iniciateGame((int)(Math.random()*admin.getGames().size()), mainStage);
		        
		      				} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (vidas.size()>0) {
						crearEnemigos();
						colisiones();
						limpiarEnemigos();
					}
					if (vidas.size()==0) {		
						background=new Image ("Pez/Imagenes/gameeOver.jpg",800,800,true,false);
						player.setImagenPlayer(new Image ("Pez/Imagenes/Vacio.png",100,100,true,false));
					}
	
					if (cantidadPuntos>=10) {
						a�adirBonus();
					}
	
					contexto.drawImage(background, 0, 0);
					contexto.drawImage(player.getImagenPlayer(),player.getPlayerPosX(),player.getPlayerPosY());
					for (int i = 0; i < vidas.size(); i++) {
						contexto.drawImage(vidas.get(i).getImageVida(),vidas.get(i).getPosVidaX(),vidas.get(i).getPosVidaY());
					}
	
					for (int i = 0; i < enemigos.size(); i++) {
	
						enemigos.get(i).moverEnemigo();
						contexto.drawImage(enemigos.get(i).getImagenPlayer(),enemigos.get(i).getPosX(),enemigos.get(i).getPosY());
					}
					for (int i = 0; i < bonus.size(); i++) {
						bonus.get(i).moverBonus();
						contexto.drawImage(bonus.get(i).getImagenPlayer(),bonus.get(i).getPosX(),bonus.get(i).getPosY());
					}
				}
			}.start();
		}//Cierre del metodo
		/**
		 * M�todo handle registra las acciones durante la ejecucion del programa.
		 */
		public void handle(KeyEvent event) {
	
			if(event.getCode().equals(KeyCode.RIGHT) && event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				player.moverDerecha();
			}
			else if(event.getCode().equals(KeyCode.LEFT) && event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				player.moverIzquierda();
	
			}
		}//Cierre del metodo
	
	
		/**
		 * M�todo a�adirBonus se encarga de generar objetos bonus de forma aleatoria.
		 */
	
		public void a�adirBonus () {
			if (bonus.size()==0&&bonus.size()<2) {
				for (int i = 0; i < 1; i++) {
					Bonus aux = new Bonus();
					bonus.add(aux);	
				}
			}
		}//Cierre del metodo.
	
	
		/**
		 * M�todo colisiones es el encargado de todas las colisiones del programa.
		 */
	
	
		public void colisiones () {
	
			try {
	
				for (int i = 0; i < bonus.size(); i++) {
	
					if (vidas.size()<5) {	
						if (colisionV(player,bonus.get(i))) {
							bonus.remove(i);
							Vida aux = new Vida();
							aux.setPosVidaX(vidas.get(vidas.size()-1).getPosVidaX()+60);
							vidas.add(aux);
						}
					}
	
				}
				for (int i = 0; i < enemigos.size(); i++) {
	
					if (colision(enemigos.get(i),player)) {
	
						enemigos.remove(i);
						vidas.remove(vidas.size()-1);
					}
				}
			} catch (IndexOutOfBoundsException e) {
				System.out.println("Murio");
			}
		}//Cierre del metodo.
	
		/**
		 * M�todo colision lanza un boolean que nos dice si el jugador cogio o no el bonus.
		 * @param bonus recibe la trayectoria del bonus.
		 * @param jugador recibe al jugador actual para verificar si la atrapo o no.
		 */
	
		public boolean colisionV(Jugador jugador,Bonus bonus ) {
	
			float x = jugador.getPlayerPosX();
			float y = jugador.getPlayerPosY();
			float Mx = jugador.getRangoPlayerX();
			float My = jugador.getRangoPlayerY();
			float m = bonus.getPosX();
			float n = bonus.getPosY();
			float Mm = bonus.getRangoX();
			float Mn = bonus.getRangoY();
	
			if((x<m&&m<Mx&&y<n&&n<My)) {
	
				return true;
			}
			if((x<m&&m<Mx&&y<Mn&&Mn<My)) {
	
				return true;
			}
			if((x<Mm&&Mm<Mx&&y<Mn&&Mn<My)) {
	
				return true;
			}
			if((x<Mm&&Mm<Mx&&y<n&&n<My)) {
	
				return true;
			}
	
			return false;
		}//Cierre del metodo
	
	
		/**
		 * M�todo colision lansa un boolean dependiendo las posiciones de los personajes.
		 * @param enemig recibe un enemigo para verificar su posicion.
		 * @param jugador recibe la posicion actual del jugador para compararla con la del enemigo.
		 */
	
	
		public boolean colision(Enemigo enemigo,Jugador jugador ) {
	
			float x = enemigo.getPosX();
			float y = enemigo.getPosY();
			float Mx = enemigo.getRangoX();
			float My = enemigo.getRangoY();
			float m = jugador.getPlayerPosX();
			float n = jugador.getPlayerPosY();
			float Mm = jugador.getRangoPlayerX();
			float Mn = jugador.getRangoPlayerY();
	
			if((x<m&&m<Mx&&y<n&&n<My)) {
	
				return true;
			}
			if((x<m&&m<Mx&&y<Mn&&Mn<My)) {
	
				return true;
			}
			if((x<Mm&&Mm<Mx&&y<Mn&&Mn<My)) {
	
				return true;
			}
			if((x<Mm&&Mm<Mx&&y<n&&n<My)) {
	
				return true;
			}
	
			return false;
		}//Cierre del metodo
	
		/**
		 * M�todo colision lansa un boolean dependiendo las posiciones de los enemigos.
		 * @param temporal recibe un enemigo para verificar su su colision con otro.
		 * @param temporal recibe un enemigo para verificar su colision con otro.
		 */
	
	
		public boolean colisionE(Enemigo enemigo,Enemigo enemigo1 ) {
	
			float x = enemigo.getPosX();
			float y = enemigo.getPosY();
			float Mx = enemigo.getRangoX();
			float My = enemigo.getRangoY();
			float m = enemigo1.getPosX();
			float n = enemigo1.getPosY();
			float Mm = enemigo1.getRangoX();
			float Mn = enemigo1.getRangoY();
	
			if((x<m&&m<Mx&&y<n&&n<My)) {
	
				return true;
			}
			if((x<m&&m<Mx&&y<Mn&&Mn<My)) {
	
				return true;
			}
			if((x<Mm&&Mm<Mx&&y<Mn&&Mn<My)) {
	
				return true;
			}
			if((x<Mm&&Mm<Mx&&y<n&&n<My)) {
	
				return true;
			}
	
			return false;
		}//Cierre del metodo 
	
	
		/**
		 * M�todo crearVidas se encarga de crear las vidas del personaje y graficarla.
		 */
	
	
		public void crearVidas () {
			for (int i = 0; i < 3; i++) {
				Vida aux = new Vida();
				vidas.add(aux);
			}
	
			vidas.get(1).setPosVidaX(vidas.get(1).getPosVidaX()+60);
			vidas.get(2).setPosVidaX(vidas.get(2).getPosVidaX()+120);
		}//Cierre del metodo
	
		/**
		 * M�todo crearEnemigos se encarga de crear los enemigos y graficarlos.
		 */
	
		public void crearEnemigos () {
	
			if (enemigos.size()<=0) {
				for (int i = 0; i < cantidadEnemigos; i++) {
					Enemigo aux = new Enemigo ();
	
					for (int j = 0; j < enemigos.size(); j++) {
						if (colisionE(enemigos.get(j),aux)) {
							aux = new Enemigo();
						}
	
					}			
					enemigos.add(aux);
				}
				cantidadEnemigos= cantidadEnemigos+8;
			}
	
		}//Cierre del metodo.
	
		/**
		 * M�todo limpiarEnemigos se encarga de limpiar el nivel una vez se ha perdido.
		 */
	
	
		public void limpiarEnemigos() {
	
	
			for (int i = 0; i < bonus.size(); i++) {
				if (bonus.get(i).getRangoY()>=10*(Math.random()*1000+500)) {
					bonus.remove(i);
				}
			}
			for (int i = 0; i < enemigos.size(); i++) {
				if (enemigos.get(i).getRangoY()>=700) {
					enemigos.remove(i);
					cantidadPuntos=cantidadPuntos+10;			
					score.setText(Float.toString(cantidadPuntos));
				}
			}
		}//Cierre del metodo
	
	}//Cierre de la clase

package Pez;
	import javafx.scene.image.Image;
	
	/**
	 * Esta clase se encarga de todo lo relacionado con el jugador principal.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez
	 * @version: 25/05/2019
	 */
	
	//Campos de la clase 
	public class Jugador {
	
		private float playerPosX;
		private float playerPosY;
		private float rangoPlayerX;
		private float rangoPlayerY;
	
		private double vidaPlayer;
		private Image imagenPlayer;
	
	
		/**
		 * Constructor para la clase de Jugador  que contiene todo sobre el jugador principal.
		 */
	
		public Jugador() {
	
			this.playerPosX = 30;
			this.playerPosY = 660;
	
			this.imagenPlayer = new Image ("Pez/Imagenes/PezDerecha.png",120,80,true,false);
	
			this.rangoPlayerX = playerPosX+60;
			this.rangoPlayerY = playerPosY+40;
			this.vidaPlayer = 1.0;
	
		}//Cierre del constructor
	
	
		/**
		 * M�todo moverDerecha le suma a la posicion en X para mover el jugador hacia la derecha.
		 */
	
		public void moverDerecha() {
			if (playerPosX<560) { 	
				imagenPlayer= new Image ("Pez/Imagenes/PezDerecha.png",120,80,true,false);
				playerPosX +=20;}
	
		}//Cierre del metodo
	
	
	
		/**
		 * M�todo moverIzquierda le resta a la posicion en X para mover el jugador hacia la izquierda.
		 */
	
		public void moverIzquierda() {
	
			if (playerPosX>20) {
				imagenPlayer= new Image ("Pez/Imagenes/PezIzquierda.png",120,80,true,false);
				playerPosX -=20;}
		}//Cierre del metodo
	
	
	
		//Getters and setters
		public Image getImagenPlayer() {
			return imagenPlayer;
		}
	
		public void setImagenPlayer(Image imagenPlayer) {
			this.imagenPlayer = imagenPlayer;
		}
	
		public double getVidaPlayer() {
			return vidaPlayer;
		}
	
		public void setVidaPlayer(double vidaPlayer) {
			this.vidaPlayer = vidaPlayer;
		}
	
		public float getPlayerPosX() {
			return playerPosX;
		}
	
		public void setPlayerPosX(float playerPosX) {
			this.playerPosX = playerPosX;
		}
	
		public float getPlayerPosY() {
			return playerPosY;
		}
	
		public void setPlayerPosY(float playerPosY) {
			this.playerPosY = playerPosY;
		}
	
		public float getRangoPlayerX() {
			return playerPosX+40;
		}
	
		public void setRangoPlayerX(float rangoPlayerX) {
			this.rangoPlayerX = rangoPlayerX;
		}
	
		public float getRangoPlayerY() {
			return playerPosY+60;
		}
	
		public void setRangoPlayerY(float rangoPlayerY) {
			this.rangoPlayerY = rangoPlayerY;
		}//Cierre de getters and setters
	
	
	
	
	}//Cierre de la clase

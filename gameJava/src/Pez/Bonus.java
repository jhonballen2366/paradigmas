package Pez;
import javafx.scene.image.Image;
	
	/**
	 * Esta clase se encarga de todo lo relacionado con los bonus que aparecen de vez en cuando en el programa.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez
	 * @version: 25/05/2019
	 */
	
	//Campos de la clase
	public class Bonus {
	
		private float posX;
		private float posY;
		private float rangoX;
		private float rangoY;
	
		private Image imagenPlayer;
	
	
		/**
		 * Constructor para la clase del Bonus que genera bonus de forma aleatoria.
		 */
	
		public Bonus() {
	
	
			this.posX=(float)Math.abs((Math.random()*(200-(800+1))+(500)));
			this.posY=-100;
	
			if (posX<0||posX>800) {
	
				this.posX=(float)Math.abs((Math.random()*(200-(800+1))+(500)));
			}
	
	
	
			this.imagenPlayer = new Image ("Pez/Imagenes/VidaPBG.png",70,70,true,false);
			this.rangoX = posX+45;
			this.rangoY = posY+45;
	
		}//Cierre del constructor 
	
		/**
		 * M�todo moverBonus se encarga de sumarle a la posicion en y del Bonus para que se vaya moviendo.
		 */
	
		public void moverBonus () {
	
			posY=posY+4;
	
		}//Cierre del metodo
	
	
		//Getters and setters 
		public float getPosX() {
			return posX;
		}
	
	
		public void setPosX(float posX) {
			this.posX = posX;
		}
	
	
		public float getPosY() {
			return posY;
		}
	
	
		public void setPosY(float posY) {
			this.posY = posY;
		}
	
	
		public float getRangoX() {
			return posX+40;
		}
	
	
		public void setRangoX(float rangoX) {
			this.rangoX = rangoX;
		}
	
	
		public float getRangoY() {
			return posY+40;
		}
	
	
		public void setRangoY(float rangoY) {
			this.rangoY = rangoY;
		}
	
	
		public Image getImagenPlayer() {
			return imagenPlayer;
		}
	
	
		public void setImagenPlayer(Image imagenPlayer) {
			this.imagenPlayer = imagenPlayer;
		}//Cierre de getters and setters
	
	}//Cierre de la clase

package Pez;
	import javafx.scene.image.Image;
	
	/**
	 * Esta clase se encarga de todo lo relacionado con los enemigos del programa.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez
	 * @version: 25/05/2019
	 */
	
	//Campos de la clase
	public class Enemigo {
	
		private float posX;
		private float posY;
		private float rangoX;
		private float rangoY;
	
		private Image imagenPlayer;
	
	
		/**
		 * Constructor para la clase de Enemigo de los enemigos del juego.
		 */
	
		public Enemigo() {
	
	
			this.posX=(float)Math.abs((Math.random()*(200-(800+1))+(500)));
			this.posY=-1*((float)(Math.random()*1500));
	
			if (posX<0||posX>800) {
	
				this.posX=(float)Math.abs((Math.random()*(200-(800+1))+(500)));
				this.posY=-1*((float)(Math.random()*500));
	
			} 
	
			if (posX<0||posX>800) {
	
				this.posX=(float)Math.abs((Math.random()*(200-(800+1))+(500)));
				this.posY=-1*((float)(Math.random()*500));
	
			}
	
	
			this.imagenPlayer = new Image ("Pez/Imagenes/BasuraFinal.png",200,150,true,false);
			this.rangoX = posX+100;
			this.rangoY = posY+75;
	
		}//Cierre del constructor
	
	
		/**
		 * M�todo moverEnemigo se encarga de sumarle a la posicion en y del enemigo para que se vaya moviendo.
		 */
	
		public void moverEnemigo () {
	
			posY=posY+4;
	
		}//Cierre del metodo
	
	
		// getters and setters
		public float getPosX() {
			return posX;
		}
	
		public void setPosX(float posX) {
			this.posX = posX;
		}
	
		public float getPosY() {
			return posY;
		}
	
		public void setPosY(float posY) {
			this.posY = posY;
		}
	
		public float getRangoX() {
			return posX+100;
		}
	
		public void setRangoX(float rangoX) {
			this.rangoX = rangoX;
		}
	
		public float getRangoY() {
			return posY+75;
		}
	
		public void setRangoY(float rangoY) {
			this.rangoY = rangoY;
		}
	
		public Image getImagenPlayer() {
			return imagenPlayer;
		}
	
		public void setImagenPlayer(Image imagenPlayer) {
			this.imagenPlayer = imagenPlayer;
		}//Cierre de getters and getters
	
	
	}//Cierre de la clase

package Pez;
	import javafx.application.Application;
	import javafx.scene.Group;
	import javafx.stage.Stage;
	
	/**
	 * Esta clase se encarga de ejecutar el programa.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez 
	 * @version: 25/05/2019
	 */
	
	//Campos de la clase 
	public class PezGames extends Application {
	
	
		/**
		 * M�todo start para iniciar el programa.
		 */
		public void start(Stage mainStage) throws Exception {
	
			mainStage.setTitle("JUEGO PEZ");	
			NivelPrincipal nivel01 = new NivelPrincipal(new Group(),660,750,mainStage);
			mainStage.setScene(nivel01);
			nivel01.runNivel();
			mainStage.show();
	
		}//Cierre del metodo
		  /**
		   	* M�todo main para iniciar el programa
			*/
		public static void main(String[] args) {
	
			launch(args);
		}//Cierre del metodo
	}//Cierre de la clase 

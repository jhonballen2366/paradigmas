package Pez;
	import javafx.scene.image.Image;
	
	/**
	 * Esta clase se encarga de todo lo relacionado con las vidas de todo el programa.
	 * @author: Jhon Jairo Ballen Agudelo
	 * @author: Valentina Calderon Barrera
	 * @author: Juan Camilo Cruz Jimenez
	 * @author: Cesar Reyes Lopez
	 * @version: 25/05/2019
	 */
	//Campos de la clase
	public class Vida {
		private float posVidaX;
		private float posVidaY;
		private Image imageVida;
		/**
		 * Constructor para la clase de Vida que contiene todo sobre las vidas del personaje principal.
		 */
		public Vida() {
	
			this.posVidaX = 200;
			this.posVidaY = 40;
			this.imageVida= new Image ("Pez/Imagenes/VidaPBG.png",70,70,true,false);
		}//Cierre del constructor
	
		/**
		 * M�todo moverVida se encarga de sumarle a la posicion en y de la vida para que se vaya moviendo.
		 */
		public void moverVida () {
	
			posVidaY=posVidaY+4;
	
		}//Cierre del metodo
	
		//Getters and setters
		public Image getImageVida() {
			return imageVida;
		}
		public void setImageVida(Image imageVida) {
			this.imageVida = imageVida;
		}
	
		public float getPosVidaX() {
			return posVidaX;
		}
		public void setPosVidaX(float posVidaX) {
			this.posVidaX = posVidaX;
		}
		public float getPosVidaY() {
			return posVidaY;
		}
		public void setPosVidaY(float posVidaY) {
			this.posVidaY = posVidaY;
		}//Cierre de getters and setters
	
	
	
	}
